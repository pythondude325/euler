#include <stdio.h>
#include <gmp.h>

int main(int argc, char **argv){
  mpz_t i, o;

  if(argc != 2){
    printf("Usage: %s <number>\n", argv[0]);
    return 1;
  }

  mpz_init_set_str(i, argv[1], 10);

  mpz_init_set_ui(o, 1);

  for(; mpz_get_ui(i) >= 2; mpz_sub_ui(i, i, 1)){
    mpz_mul(o, o, i);
  }

  mpz_out_str(stdout, 10, o);
  printf("\n");

  mpz_clear(o);
  mpz_clear(i);

  return 0;
}