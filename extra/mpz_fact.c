#include <stdio.h>
#include <gmp.h>

int factorial(mpz_t output, mpz_t input){
  mpz_t i;

  mpz_init_set(i, input);

  mpz_set_ui(output, 1);

  for(; mpz_get_ui(i) >= 2; mpz_sub_ui(i, i, 1)){
    mpz_mul(output, output, i);
  }

  return 0;
}

int main(int argc, char **argv){
  mpz_t n;

  if(argc != 2){
    printf("Usage: %s <number>\n", argv[0]);
    return 1;
  }

  mpz_init_set_str(n, argv[1], 10);

  factorial(n, n);

  mpz_out_str(stdout, 10, n);
  printf("\n");

  mpz_clear(n);

  return 0;
}