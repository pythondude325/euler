#include <stdio.h>
#include <stdbool.h>
#include <math.h>

int main(){
  for(int i = 1; true; i++){
    int n = i*(i+1)/2;
    double root_n = sqrt(n);
    
    // This goes through all whole numbers between 1 and sqrt(n) and checks for divisors.
    int divisors = 0;
    for(int d = 1; d <= root_n; d++){
      if(n % d == 0){
        divisors++;
      }
    }
    divisors *= 2; // The previous loop only went through half of divisors.

    if(root_n == floor(root_n)){ // The loop also doesn't count perfect squares
      divisors++;
    }

    if(divisors > 500){
      printf("Triangle number %d is %d and has %d divisors.\n", i, n, divisors);
      break;
    }
  }

  return 0;
}