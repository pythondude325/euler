#!/usr/bin/env python

import sys

total = 0
for line in sys.stdin.readlines():
  total += int(line)

print "Total: %s" % (total)