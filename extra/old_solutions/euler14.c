#include <stdio.h>

int steps_in_sequence(long n){
  int steps = 0;

  while(n > 1){
    if(n % 2 == 0){
      n = n / 2;
    } else {
      n = 3 * n + 1;
    }
    steps++;
  }

  return steps;
}

int main(){
  int number_max_steps, max_steps = 0;

  for(int i = 1; i < 1000000; i++){
    int steps = steps_in_sequence(i);

    if(steps > max_steps){
      number_max_steps = i;
      max_steps = steps;
      printf("%d requires %d steps.\n", number_max_steps, max_steps);
    }
  }

  return 0;
}