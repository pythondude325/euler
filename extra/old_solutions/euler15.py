import math

def lattice_paths(x, y):
  return math.factorial(x+y)/(math.factorial(x)*math.factorial(y))

print "There are %d lattice paths" % (lattice_paths(20, 20))
