#!/usr/bin/env python3

ones = ["one", "two", "three", "four", "five", "six", "seven", "eight", "nine"]
ntys = ["twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"]

tens = ["ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"]

def write_num(n):
    s = ""

    p_hundreds = n % 1000 >= 100
    if p_hundreds:
        s += ones[n // 100 - 1] + " hundred "

        if (n % 100) > 0:
            s += "and "

    p_ntys = n % 100 >= 20
    if p_ntys:
        s += ntys[(n % 100) // 10 - 2]

    p_tens = 20 > (n % 100) >= 10
    if not(p_ntys) and p_tens:
        s += tens[(n % 100) - 10]

    p_ones = n % 10 >= 1
    if not(p_tens) and p_ones:
        if(p_ntys):
            s += " "
        s += ones[(n % 10) - 1]

    return s


letter_count = 0

for a in range(1, 1000):
    s = write_num(a)
    print(s)
    s = s.replace(" ", "")
    print(s)
    letter_count += len(s)


letter_count += len("onethousand")

print(letter_count)
