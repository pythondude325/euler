fn nth_lexicographic_permutation<T: Ord>(items: &mut [T], mut n: usize) {
    let l = items.len();
    let mut f: usize = (2..=l).product();

    for d in 0..(l - 1) {
        items[d..].sort();
        f /= l - d;
        items.swap(d, d + n / f);
        n %= f;
    }
}

fn main() {
    let mut i = (0..=9).collect::<Vec<_>>();
    
    nth_lexicographic_permutation(&mut i, 999_999);
    
    for d in i {
        print!("{}", d);
    }
    println!();
}
