#!/usr/bin/env python3

import math

golden_ratio = (1 + math.sqrt(5)) / 2

fibonacci_asymtotic = math.log10(golden_ratio)

i = math.floor(1000 / fibonacci_asymtotic - 2)
print(i)
