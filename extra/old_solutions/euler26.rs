extern crate primes;
use primes::PrimeIterator;

fn gcd(a: i64, b: i64) -> i64 {
    // From https://en.wikipedia.org/wiki/Greatest_common_divisor#Euclid.27s_algorithm
    use std::cmp::Ordering;

    match a.cmp(&b) {
        Ordering::Equal => a,
        Ordering::Greater => gcd(a - b, b),
        Ordering::Less => gcd(a, b - a),
    }
}

fn multiplicative_order(a: i64, n: i64) -> i64 {
    assert!(gcd(a, n) == 1);

    let mut c = 1;
    for k in 1.. {
        c = (a * c) % n;
        if c == 1 {
            return k;
        }
    }

    unreachable!() // Unreachable because of the assertion that a and n are coprime
}

fn main() {
    println!(
        "{}",
        PrimeIterator::new()
            .take_while(|&d| d < 1000)
            .skip(3)
            .max_by_key(|&d| multiplicative_order(10, d))
            .unwrap()
    );
}
