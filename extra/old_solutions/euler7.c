#include <stdio.h>
#include <math.h>
#include <stdbool.h>
#include <stdlib.h>

#define num_of_primes 10001

bool isPrime(int num, int primelist[]){
  double num_root = sqrt(num);
  int test_number_index = 0;
  
  while(true){
    if(test_number_index > num_of_primes){
      return false;
    }
    
    int current_prime_test = primelist[test_number_index];
    
    if(current_prime_test == 0){
      return true;
    }
    
    if(current_prime_test > num_root){
      return true;
    }
    
    if(num % primelist[test_number_index] == 0){
      return false;
    }
    
    test_number_index++;
  }
}

int main(){
  int primelist[num_of_primes + 1] = {0};
  int current_prime_count = 0;
  int current_test_number = 2;
  
  while(current_prime_count < num_of_primes){
    if(isPrime(current_test_number, primelist)){
      current_prime_count++;
      primelist[current_prime_count - 1] = current_test_number;
      printf("Prime %d: %d\n", current_prime_count, current_test_number);
    }
    current_test_number++;
  }
  
  free(primelist);
  return 0;
}
