// Most of this code is reused from problem 7

#include <stdio.h>
#include <math.h>
#include <stdbool.h>
#include <stdlib.h>

// Precalculated value about equal to li(2,000,000)
#define num_of_primes 150000

bool isPrime(int num, long primelist[]){
  double num_root = sqrt(num);
  int test_number_index = 0;
  nn
  for(test_number_index = 0; test_number_index <= num_of_primes; test_number_index++){
    int current_prime_test = primelist[test_number_index];
    
    if(current_prime_test == 0) return true; // A zero in the list means there are no more prime numbers
    
    if(current_prime_test > num_root) return true; // No need to check prime numbers past sqrt(n)
    
    if(num % primelist[test_number_index] == 0) return false; // Fail if divisible
  }

  return true; // Array out of bounds; No more primes
}

int main(){
  long primelist[num_of_primes + 1] = {0};
  int current_prime_count = 0;
  int current_test_number = 2;

  long prime_sum = 0;
  
  while(current_prime_count < num_of_primes){
    if(isPrime(current_test_number, primelist)){
      current_prime_count++;
      primelist[current_prime_count - 1] = current_test_number;
      // printf("Prime %d: %d\n", current_prime_count, current_test_number);

      prime_sum += current_test_number;
    }
    
    if(current_test_number < 2000000){
      current_test_number++;
    } else {
      break;
    }
  }
  
  printf("Sum of all primes below 2M: %ld\n", prime_sum);

  return 0;
}
