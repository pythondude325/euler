fn gcd(a: i64, b: i64) -> i64 {
    // From https://en.wikipedia.org/wiki/Greatest_common_divisor#Euclid.27s_algorithm
    use std::cmp::Ordering;

    match a.cmp(&b) {
        Ordering::Equal => a,
        Ordering::Greater => gcd(a - b, b),
        Ordering::Less => gcd(a, b - a)
    }
}

fn lcm(a: i64, b: i64) -> i64 {
    a * b / gcd(a, b)
}

fn is_coprime(a: i64, b: i64) -> bool {
    gcd(a, b) == 1
}

fn charmichael_function(n: i64) -> i64 {
    (1..).find(|&m| (1..n).filter(|&a| is_coprime(a, n)).all(|a| mod_exp(a, m, n) == 1)).unwrap()
}

fn mod_exp(b: i32, e: i32, m: i32) -> i32 {
    let mut c = 1;
    for _ in 0..e {
        c = (b * c) % m;
    }
    c
}
