### A Pluto.jl notebook ###
# v0.19.8

using Markdown
using InteractiveUtils

# ╔═╡ 8316f235-2c1e-41a9-94a0-7a501d8dc058
using Primes

# ╔═╡ 38e08b39-86b6-4738-99c7-0bdf772c6b12
digits(x) = floor(Int, log10(x)) + 1

# ╔═╡ 898dae11-2723-4fca-8079-324e71a2934d
function generate_single_trunc_primes()
	s = [[2,3,5,7]]
	i = 1
	while !isempty(s[i])
		S = []
		for p=s[i]
			for d=0:9
				n = p * 10 + d
				if isprime(n)
					push!(S, n)
				end
			end
		end
		push!(s, S)
		i += 1
	end
	reduce(union, Set.(s))
end

# ╔═╡ 55207581-2953-47ac-a7d9-ee7e6623c349
const single_trunc_primes = generate_single_trunc_primes()

# ╔═╡ ce7ce3a4-ebcd-4888-a54b-c5bb59517eed
function check_opp(p) 
	all(isprime(p % 10^d) for d=1:digits(p))
end

# ╔═╡ 0695e79c-377f-45ba-a63d-15ce9122122d
sum(factorial.(1:9))

# ╔═╡ 3d58e64e-7768-4eb4-93e3-dbfe65af8a54
const result = sum(filter(check_opp, single_trunc_primes)) - sum([2,3,5,7])

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
Primes = "27ebfcd6-29c5-5fa9-bf4b-fb8fc14df3ae"

[compat]
Primes = "~0.5.3"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.3"
manifest_format = "2.0"

[[deps.IntegerMathUtils]]
git-tree-sha1 = "f366daebdfb079fd1fe4e3d560f99a0c892e15bc"
uuid = "18e54dd8-cb9d-406c-a71d-865a43cbb235"
version = "0.1.0"

[[deps.Primes]]
deps = ["IntegerMathUtils"]
git-tree-sha1 = "311a2aa90a64076ea0fac2ad7492e914e6feeb81"
uuid = "27ebfcd6-29c5-5fa9-bf4b-fb8fc14df3ae"
version = "0.5.3"
"""

# ╔═╡ Cell order:
# ╠═8316f235-2c1e-41a9-94a0-7a501d8dc058
# ╠═38e08b39-86b6-4738-99c7-0bdf772c6b12
# ╠═898dae11-2723-4fca-8079-324e71a2934d
# ╠═55207581-2953-47ac-a7d9-ee7e6623c349
# ╠═ce7ce3a4-ebcd-4888-a54b-c5bb59517eed
# ╠═0695e79c-377f-45ba-a63d-15ce9122122d
# ╠═3d58e64e-7768-4eb4-93e3-dbfe65af8a54
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
