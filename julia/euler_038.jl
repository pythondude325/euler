### A Pluto.jl notebook ###
# v0.19.8

using Markdown
using InteractiveUtils

# ╔═╡ 49bfd764-fd16-4f87-9179-47d66035815c
const pandigital_set = [1,2,3,4,5,6,7,8,9]

# ╔═╡ 652c650c-ff05-11ec-2705-3f1797222b01
function try_number(k)
	ds = []
	n = 1
	while length(ds) < 9
		m = k * n
		append!(ds, reverse(digits(m)))
		n += 1
	end
	if length(ds) == 9
		if sort(ds) == pandigital_set
			evalpoly(10, reverse(ds))
		else
			0
		end
	else
		0
	end
end

# ╔═╡ 70f06712-d4c3-4ea4-aaf2-47ca74721d8f
const result = maximum(try_number.(1:9999))

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.3"
manifest_format = "2.0"

[deps]
"""

# ╔═╡ Cell order:
# ╠═49bfd764-fd16-4f87-9179-47d66035815c
# ╠═652c650c-ff05-11ec-2705-3f1797222b01
# ╠═70f06712-d4c3-4ea4-aaf2-47ca74721d8f
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
