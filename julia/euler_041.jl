### A Pluto.jl notebook ###
# v0.19.8

using Markdown
using InteractiveUtils

# ╔═╡ 4c02f760-fefd-11ec-3544-c5c18e8095e8
using Primes

# ╔═╡ 5bb78ae0-09a3-4010-adcb-cdc17df303ce
using LinearAlgebra

# ╔═╡ cb88a539-1de7-4170-b976-a65e517313d8
function permutation_matrix(w, i)::Matrix{Bool}  
	function swap_matrix(w, i)
		m = Matrix{Bool}(I, w,w)
		m[i,i] = 0
		m[i,1] = 1
		m[1,1] = 0
		m[1,i] = 1
		m
	end
	
	extend_matrix(m, h,w) =
		[
			Matrix{Bool}(I,h-size(m, 1),w-size(m,2)) falses(h-size(m,1),size(m, 2));
			falses(size(m,1), w-size(m,2)) m
		]
	
	if w > 1
		swap_matrix(w, (i % w)+1) * extend_matrix(permutation_matrix(w-1, i÷w), w, w)
	else
		swap_matrix(1, 1)
	end
end

# ╔═╡ ba96b849-745b-4978-92cc-c04c1735a8f5
n_pandigital_primes(n) = 
	filter(isprime, [evalpoly(10, perm * (1:n)) for perm=permutation_matrix.(n, 0:(factorial(n)-1))])

# ╔═╡ eabb4c51-b345-43bc-9d81-14e2baaa9f5d
const result = maximum(reduce(vcat, n_pandigital_primes.(1:7)))

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
LinearAlgebra = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"
Primes = "27ebfcd6-29c5-5fa9-bf4b-fb8fc14df3ae"

[compat]
Primes = "~0.5.3"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.3"
manifest_format = "2.0"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"

[[deps.IntegerMathUtils]]
git-tree-sha1 = "f366daebdfb079fd1fe4e3d560f99a0c892e15bc"
uuid = "18e54dd8-cb9d-406c-a71d-865a43cbb235"
version = "0.1.0"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"

[[deps.Primes]]
deps = ["IntegerMathUtils"]
git-tree-sha1 = "311a2aa90a64076ea0fac2ad7492e914e6feeb81"
uuid = "27ebfcd6-29c5-5fa9-bf4b-fb8fc14df3ae"
version = "0.5.3"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl", "OpenBLAS_jll"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"
"""

# ╔═╡ Cell order:
# ╠═4c02f760-fefd-11ec-3544-c5c18e8095e8
# ╠═5bb78ae0-09a3-4010-adcb-cdc17df303ce
# ╠═cb88a539-1de7-4170-b976-a65e517313d8
# ╠═ba96b849-745b-4978-92cc-c04c1735a8f5
# ╠═eabb4c51-b345-43bc-9d81-14e2baaa9f5d
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
