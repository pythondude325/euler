### A Pluto.jl notebook ###
# v0.19.8

using Markdown
using InteractiveUtils

# ╔═╡ 285d2086-b5c2-4f61-ab61-112e911ec496
using Permutations

# ╔═╡ 8ac1bf38-bf32-47ea-b07e-92848eb02b15
undigits(ds) = evalpoly(10, reverse(ds))

# ╔═╡ d5830e31-f21e-4d26-8209-2bd44da0d687
function test(x)
	all(undigits(@view x[i:i+2]) % p == 0 for (i,p)=zip(2:8, [2,3,5,7,11,13,17]))
end

# ╔═╡ 9fad98e3-9084-4759-89e6-41a78b4b7ac7
const pandigital_numbers = [Permutation(10,p).data .% 10 for p=1:factorial(10)]

# ╔═╡ 21d4df80-3a87-4108-a6e2-3e0a004685e9
const result = filter(test, pandigital_numbers) .|> undigits |> sum

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
Permutations = "2ae35dd2-176d-5d53-8349-f30d82d94d4f"

[compat]
Permutations = "~0.4.14"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.3"
manifest_format = "2.0"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.Combinatorics]]
git-tree-sha1 = "08c8b6831dc00bfea825826be0bc8336fc369860"
uuid = "861a8166-3701-5b0c-9a16-15d98fcdc6aa"
version = "1.0.2"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"

[[deps.Permutations]]
deps = ["Combinatorics", "LinearAlgebra", "Random"]
git-tree-sha1 = "f3e7100a00388b602c7d88a7e3cf12539370f28d"
uuid = "2ae35dd2-176d-5d53-8349-f30d82d94d4f"
version = "0.4.14"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl", "OpenBLAS_jll"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"
"""

# ╔═╡ Cell order:
# ╠═285d2086-b5c2-4f61-ab61-112e911ec496
# ╠═8ac1bf38-bf32-47ea-b07e-92848eb02b15
# ╠═d5830e31-f21e-4d26-8209-2bd44da0d687
# ╠═9fad98e3-9084-4759-89e6-41a78b4b7ac7
# ╠═21d4df80-3a87-4108-a6e2-3e0a004685e9
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
