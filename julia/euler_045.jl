### A Pluto.jl notebook ###
# v0.19.8

using Markdown
using InteractiveUtils

# ╔═╡ febc4846-ff2c-11ec-3278-ff2f3dabbf5d
H(n) = n*(2n-1)

# ╔═╡ 6b053872-4b69-499f-be9f-7fc5ce0edb83
function ptest(x)
	y = sqrt(24x + 1)
	z = floor(Int, y)
	y == z && z % 6 == 5
end

# ╔═╡ 80df9824-7945-455b-bb9c-913ae3f173db
function search()
	n = 144
	while true
		if ptest(H(n))
			return H(n)
		end
		n += 1
	end
end

# ╔═╡ 9b0f9596-bc07-42b1-8a45-903346157db7
search()

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.3"
manifest_format = "2.0"

[deps]
"""

# ╔═╡ Cell order:
# ╠═febc4846-ff2c-11ec-3278-ff2f3dabbf5d
# ╠═6b053872-4b69-499f-be9f-7fc5ce0edb83
# ╠═80df9824-7945-455b-bb9c-913ae3f173db
# ╠═9b0f9596-bc07-42b1-8a45-903346157db7
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
