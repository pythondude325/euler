### A Pluto.jl notebook ###
# v0.19.8

using Markdown
using InteractiveUtils

# ╔═╡ 727cedbc-ff1e-11ec-39ab-1b446457b3de
using Primes

# ╔═╡ 5ec4f248-4672-45f4-82dd-a65604799a64
function test(n)
	for double_square=(2*n*n for n=1:typemax(Int64))
		d = n - double_square
		if d < 2
			return false
		end
		
		if isprime(d)
			return true
		end
	end
	error("Not enough double squares")
end

# ╔═╡ 7e30bc7c-ad4d-432f-ba2d-b1cc0c8b13b5
function search()
	i = 3
	while true
		if !isprime(i) && !test(i)
			return i
		end
		i += 2
	end
end

# ╔═╡ c4346a70-ef34-416e-9d44-f2143756d305
const result = search()

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
Primes = "27ebfcd6-29c5-5fa9-bf4b-fb8fc14df3ae"

[compat]
Primes = "~0.5.3"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.3"
manifest_format = "2.0"

[[deps.IntegerMathUtils]]
git-tree-sha1 = "f366daebdfb079fd1fe4e3d560f99a0c892e15bc"
uuid = "18e54dd8-cb9d-406c-a71d-865a43cbb235"
version = "0.1.0"

[[deps.Primes]]
deps = ["IntegerMathUtils"]
git-tree-sha1 = "311a2aa90a64076ea0fac2ad7492e914e6feeb81"
uuid = "27ebfcd6-29c5-5fa9-bf4b-fb8fc14df3ae"
version = "0.5.3"
"""

# ╔═╡ Cell order:
# ╠═727cedbc-ff1e-11ec-39ab-1b446457b3de
# ╠═5ec4f248-4672-45f4-82dd-a65604799a64
# ╠═7e30bc7c-ad4d-432f-ba2d-b1cc0c8b13b5
# ╠═c4346a70-ef34-416e-9d44-f2143756d305
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
