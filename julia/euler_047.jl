### A Pluto.jl notebook ###
# v0.19.8

using Markdown
using InteractiveUtils

# ╔═╡ 9cc80202-027e-43da-a7ff-0fcb4daf5ff0
using Primes

# ╔═╡ bb572be7-4c9a-4ae5-b4bd-48ad1c3b61a7
factor(Set, 644)

# ╔═╡ 1b39ef5f-362a-4d6b-b0c0-01dd1df2a31c
const ps = primes(10_000_000)

# ╔═╡ 7f3af072-ff18-11ec-2a12-79096473a2a6
function search()
	i = 1
	
	consecutive = 0

	while consecutive < 4
		distinct_factors = length(factor(Set, i))
		if distinct_factors != 4
			consecutive = 0
			i += 4
		else
			consecutive += 1
			i += 1
		end
	end

	i - 4
end

# ╔═╡ e803345c-f4cf-4acb-8396-f1ea59c074e8
const result = search()

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
Primes = "27ebfcd6-29c5-5fa9-bf4b-fb8fc14df3ae"

[compat]
Primes = "~0.5.3"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.3"
manifest_format = "2.0"

[[deps.IntegerMathUtils]]
git-tree-sha1 = "f366daebdfb079fd1fe4e3d560f99a0c892e15bc"
uuid = "18e54dd8-cb9d-406c-a71d-865a43cbb235"
version = "0.1.0"

[[deps.Primes]]
deps = ["IntegerMathUtils"]
git-tree-sha1 = "311a2aa90a64076ea0fac2ad7492e914e6feeb81"
uuid = "27ebfcd6-29c5-5fa9-bf4b-fb8fc14df3ae"
version = "0.5.3"
"""

# ╔═╡ Cell order:
# ╠═9cc80202-027e-43da-a7ff-0fcb4daf5ff0
# ╠═bb572be7-4c9a-4ae5-b4bd-48ad1c3b61a7
# ╠═1b39ef5f-362a-4d6b-b0c0-01dd1df2a31c
# ╠═7f3af072-ff18-11ec-2a12-79096473a2a6
# ╠═e803345c-f4cf-4acb-8396-f1ea59c074e8
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
