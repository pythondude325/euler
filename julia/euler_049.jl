### A Pluto.jl notebook ###
# v0.19.8

using Markdown
using InteractiveUtils

# ╔═╡ 0e3e1c80-ff17-11ec-3e2e-dd4a254f9c78
using Primes

# ╔═╡ 1f490f26-e291-4f7c-9512-c7ecca8e50d6
function test_seq(a, b, c)
	da = sort(digits(a))
	db = sort(digits(b))
	dc = sort(digits(c))
	
	da == db && da == dc
	
	# println("$a $b $c")
end

# ╔═╡ 760ca964-f59e-4cac-83c4-fa14234e8126
function search()
	for p=primes(100, 10000 ÷ 3)
		for i=1:(10000-p)÷2
			b = p + i
			if !isprime(b)
				continue
			end
			c = b + i
			if !isprime(c)
				continue
			end

			if test_seq(p, b, c)
				println("$p + $i n; $p$b$c")
			end
		end
	end
end

# ╔═╡ 079ce64f-9a6a-4d6a-9205-506046186b1a
search()

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
Primes = "27ebfcd6-29c5-5fa9-bf4b-fb8fc14df3ae"

[compat]
Primes = "~0.5.3"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.3"
manifest_format = "2.0"

[[deps.IntegerMathUtils]]
git-tree-sha1 = "f366daebdfb079fd1fe4e3d560f99a0c892e15bc"
uuid = "18e54dd8-cb9d-406c-a71d-865a43cbb235"
version = "0.1.0"

[[deps.Primes]]
deps = ["IntegerMathUtils"]
git-tree-sha1 = "311a2aa90a64076ea0fac2ad7492e914e6feeb81"
uuid = "27ebfcd6-29c5-5fa9-bf4b-fb8fc14df3ae"
version = "0.5.3"
"""

# ╔═╡ Cell order:
# ╠═0e3e1c80-ff17-11ec-3e2e-dd4a254f9c78
# ╠═1f490f26-e291-4f7c-9512-c7ecca8e50d6
# ╠═760ca964-f59e-4cac-83c4-fa14234e8126
# ╠═079ce64f-9a6a-4d6a-9205-506046186b1a
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
