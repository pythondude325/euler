### A Pluto.jl notebook ###
# v0.19.8

using Markdown
using InteractiveUtils

# ╔═╡ d3535368-ff11-11ec-3a61-991af290ef98
using Primes

# ╔═╡ 64c40d7a-67f0-47f0-b86e-87eb71e98026
const ps = primes(1_000_000)

# ╔═╡ 683fdba0-38d2-4bf9-b38c-405f160a2742
const largest_prime = ps[end]

# ╔═╡ d798c297-bfdc-4140-ae42-d14df4747647
function test()
	for l=reverse(1:550)
		s = sum(@view ps[1:l])

		for i=1:length(ps)
			
			if s > largest_prime
				break
				#continue
			end

			if isprime(s)
				return s
			end

			s -= ps[i]
			s += ps[i+l]
		end
	end
end

# ╔═╡ 8a4fc176-d461-4b1a-a7c9-f4bdf2e61b47
const result = test()

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
Primes = "27ebfcd6-29c5-5fa9-bf4b-fb8fc14df3ae"

[compat]
Primes = "~0.5.3"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.3"
manifest_format = "2.0"

[[deps.IntegerMathUtils]]
git-tree-sha1 = "f366daebdfb079fd1fe4e3d560f99a0c892e15bc"
uuid = "18e54dd8-cb9d-406c-a71d-865a43cbb235"
version = "0.1.0"

[[deps.Primes]]
deps = ["IntegerMathUtils"]
git-tree-sha1 = "311a2aa90a64076ea0fac2ad7492e914e6feeb81"
uuid = "27ebfcd6-29c5-5fa9-bf4b-fb8fc14df3ae"
version = "0.5.3"
"""

# ╔═╡ Cell order:
# ╠═d3535368-ff11-11ec-3a61-991af290ef98
# ╠═64c40d7a-67f0-47f0-b86e-87eb71e98026
# ╠═683fdba0-38d2-4bf9-b38c-405f160a2742
# ╠═d798c297-bfdc-4140-ae42-d14df4747647
# ╠═8a4fc176-d461-4b1a-a7c9-f4bdf2e61b47
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
