### A Pluto.jl notebook ###
# v0.19.8

using Markdown
using InteractiveUtils

# ╔═╡ 7d17ace6-ff3a-11ec-24f3-6f4b4d0a6f55
using Primes

# ╔═╡ 91bcadab-aef4-4613-affb-17d9c96ced91
digit_len(x) = floor(Int, log10(x))+1

# ╔═╡ 1a9a4658-d0d2-4e65-8365-0bb008aefa62
undigits(x; base=10) = evalpoly(10, x)

# ╔═╡ d301c6db-f98c-470f-915e-55443a1a0b97
function gap_number(mask, n)
	acc = 0
	for i=1:digit_len(mask)
		d = mask ÷ 10^(i-1) % 10
		if d == 0
			continue
		end
		acc += 10^(i-1) * (n % 10)
		n ÷= 10
	end
	acc
end

# ╔═╡ 871d48af-f7b7-450c-887d-dfe07a110788
function search()
	d = 2
	while d < 10
		for c=2:2:2^d-1
			m = undigits(digits(c, base=2))
			um = undigits(digits(~c & (2^d-1), base=2))

			gaps = d-count_ones(c)
			for f=10^(gaps-1):10^gaps-1
				fill_num = gap_number(um, f)

				nums_found = [fill_num + m * d for d=0:9]
				if sum(isprime.(nums_found)) >= 8
					ps = filter(isprime, nums_found)
					println("on $fill_num, $m primes $ps")
					return ps[1]
				end
			end
		end
	end
end

# ╔═╡ a011247c-c74d-4d68-aa22-93c1a06822aa
const result = search()

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
Primes = "27ebfcd6-29c5-5fa9-bf4b-fb8fc14df3ae"

[compat]
Primes = "~0.5.3"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.3"
manifest_format = "2.0"

[[deps.IntegerMathUtils]]
git-tree-sha1 = "f366daebdfb079fd1fe4e3d560f99a0c892e15bc"
uuid = "18e54dd8-cb9d-406c-a71d-865a43cbb235"
version = "0.1.0"

[[deps.Primes]]
deps = ["IntegerMathUtils"]
git-tree-sha1 = "311a2aa90a64076ea0fac2ad7492e914e6feeb81"
uuid = "27ebfcd6-29c5-5fa9-bf4b-fb8fc14df3ae"
version = "0.5.3"
"""

# ╔═╡ Cell order:
# ╠═7d17ace6-ff3a-11ec-24f3-6f4b4d0a6f55
# ╠═91bcadab-aef4-4613-affb-17d9c96ced91
# ╠═1a9a4658-d0d2-4e65-8365-0bb008aefa62
# ╠═d301c6db-f98c-470f-915e-55443a1a0b97
# ╠═871d48af-f7b7-450c-887d-dfe07a110788
# ╠═a011247c-c74d-4d68-aa22-93c1a06822aa
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
