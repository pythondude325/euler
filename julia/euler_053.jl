### A Pluto.jl notebook ###
# v0.19.8

using Markdown
using InteractiveUtils

# ╔═╡ d84b4fc4-0204-11ed-2d84-11604f55d480
log_factorial(x) = sum(log.(1:x))

# ╔═╡ 6680ee94-4a7e-4da0-a46c-dc7b0e353c47
log_comb(n, r) = log_factorial(n) - log_factorial(r) - log_factorial(n - r)

# ╔═╡ 788bd7c0-548d-4a08-bc48-0c8e420ee4fb
const target = log(1_000_000)

# ╔═╡ ba2c7217-e43e-4eeb-b0df-91bd2ee65f03
function search()
	c = 0
	for n=1:100
		for r=1:n
			if log_comb(n,r) > target
				c += 1
			end
		end
	end
	c
end

# ╔═╡ 9023c91c-2434-40e8-bfab-335237153312
const result = search()

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.3"
manifest_format = "2.0"

[deps]
"""

# ╔═╡ Cell order:
# ╠═d84b4fc4-0204-11ed-2d84-11604f55d480
# ╠═6680ee94-4a7e-4da0-a46c-dc7b0e353c47
# ╠═788bd7c0-548d-4a08-bc48-0c8e420ee4fb
# ╠═ba2c7217-e43e-4eeb-b0df-91bd2ee65f03
# ╠═9023c91c-2434-40e8-bfab-335237153312
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
