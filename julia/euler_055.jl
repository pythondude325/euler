### A Pluto.jl notebook ###
# v0.19.8

using Markdown
using InteractiveUtils

# ╔═╡ 093338d6-0223-11ed-1dec-7769bdb77b3f
function test(n)
	for i=1:50
		d = digits(n)
		rd = reverse(d)
		if i > 1 && d == rd
			return false
		end
		n += evalpoly(10, rd)
	end
	return true

end

# ╔═╡ 4855d4e8-e67f-440c-9ca9-bbb3ecd45c5e
const result = sum(test.(UInt64.(1:9999)))

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.3"
manifest_format = "2.0"

[deps]
"""

# ╔═╡ Cell order:
# ╠═093338d6-0223-11ed-1dec-7769bdb77b3f
# ╠═4855d4e8-e67f-440c-9ca9-bbb3ecd45c5e
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
