### A Pluto.jl notebook ###
# v0.19.8

using Markdown
using InteractiveUtils

# ╔═╡ c42117cb-f360-4d1e-9014-8e266739ca4d
function digit_sum(x)
	s = 0
	while x > 0
		s += x % 10
		x ÷= 10
	end
	s
end

# ╔═╡ eee6e5fe-0224-11ed-128b-9b2d873e2b99
function search(a)
	sums = Int64[]
	t = big(1)
	Int64[digit_sum(t *= a) for b=1:99]
end

# ╔═╡ 7973f792-dbb5-42de-88c4-dd43b0ac2d5f
const result = findmax(reduce(hcat, search.(1:99)))[1]

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.3"
manifest_format = "2.0"

[deps]
"""

# ╔═╡ Cell order:
# ╠═c42117cb-f360-4d1e-9014-8e266739ca4d
# ╠═eee6e5fe-0224-11ed-128b-9b2d873e2b99
# ╠═7973f792-dbb5-42de-88c4-dd43b0ac2d5f
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
