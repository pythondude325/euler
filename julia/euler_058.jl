### A Pluto.jl notebook ###
# v0.19.8

using Markdown
using InteractiveUtils

# ╔═╡ 834e1929-5bfb-4712-b9f9-d76acd4aa17f
using Primes

# ╔═╡ 7e550605-fb6a-4a33-bc9e-79e812781b26
function search()
	prime_count = 0
	number_count = 1

	n = 1
	inc = 2
	while prime_count < 4 || prime_count / number_count > .1
		n += inc
		prime_count += isprime(n)
		n += inc
		prime_count += isprime(n)
		n += inc
		prime_count += isprime(n)
		n += inc
		inc += 2
		number_count += 4
	end
	inc - 1
end

# ╔═╡ 9f9876ff-93d9-4ab4-b076-1723e3da5c3b
const result = search()

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
Primes = "27ebfcd6-29c5-5fa9-bf4b-fb8fc14df3ae"

[compat]
Primes = "~0.5.3"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.3"
manifest_format = "2.0"

[[deps.IntegerMathUtils]]
git-tree-sha1 = "f366daebdfb079fd1fe4e3d560f99a0c892e15bc"
uuid = "18e54dd8-cb9d-406c-a71d-865a43cbb235"
version = "0.1.0"

[[deps.Primes]]
deps = ["IntegerMathUtils"]
git-tree-sha1 = "311a2aa90a64076ea0fac2ad7492e914e6feeb81"
uuid = "27ebfcd6-29c5-5fa9-bf4b-fb8fc14df3ae"
version = "0.5.3"
"""

# ╔═╡ Cell order:
# ╠═834e1929-5bfb-4712-b9f9-d76acd4aa17f
# ╠═7e550605-fb6a-4a33-bc9e-79e812781b26
# ╠═9f9876ff-93d9-4ab4-b076-1723e3da5c3b
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
