### A Pluto.jl notebook ###
# v0.19.8

using Markdown
using InteractiveUtils

# ╔═╡ 4596e0a2-7a47-4437-b47a-4671b145e358
const cipher_file = download("https://projecteuler.net/project/resources/p059_cipher.txt")

# ╔═╡ 33257ec2-025c-11ed-1f76-fb7ef284bfef
const cipher = parse.(UInt8, split(String(read(cipher_file)), ","))

# ╔═╡ 14ec8010-1011-4cd7-90ab-a4a0537967cd
decode(password) = cipher .⊻ Iterators.take(Iterators.cycle(password), length(cipher)) |> String

# ╔═╡ 323e3e46-44a5-4b2d-8e1a-4086ae6a09e3
find_possible_key(cipher_section) =
	reshape(
		mapslices(
			s -> all(∈(0x20:0x7e), s),
			cipher_section .⊻ (0x61:0x7a)',
			dims=1),
		:
	) |> findall

# ╔═╡ bfd4006b-d5fd-4b32-afac-a10fb36d6232
function find_possible_key_letters(len)
	(o -> find_possible_key(cipher[o:len:end]) .+ 0x60 .|> Char).(1:len)
end

# ╔═╡ e76c7623-ed1d-4297-b39a-af2b1b1f9b1f
function search()
	for key=Iterators.product(find_possible_key_letters(3)...)
		println("$(String(collect(key))): $(decode(UInt8.(key))[1:20])")
	end
end

# ╔═╡ 4e778906-face-4dc3-8a47-0b14e533c2ba
# search()

# ╔═╡ 85007a93-7f33-4169-b04a-d33f120a0917


# ╔═╡ 5f6ff7a1-4165-4ce3-9c3a-fa0e47859c11
"exp" |> collect .|> UInt8 |> decode

# ╔═╡ 4d92eb27-f7c8-4f8a-8eb0-3d0a11463b2c
const result = "exp" |> collect .|> UInt8 |> decode |> collect .|> UInt8 |> sum |> Int64

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.3"
manifest_format = "2.0"

[deps]
"""

# ╔═╡ Cell order:
# ╠═4596e0a2-7a47-4437-b47a-4671b145e358
# ╠═33257ec2-025c-11ed-1f76-fb7ef284bfef
# ╠═14ec8010-1011-4cd7-90ab-a4a0537967cd
# ╠═323e3e46-44a5-4b2d-8e1a-4086ae6a09e3
# ╠═bfd4006b-d5fd-4b32-afac-a10fb36d6232
# ╠═e76c7623-ed1d-4297-b39a-af2b1b1f9b1f
# ╠═4e778906-face-4dc3-8a47-0b14e533c2ba
# ╠═85007a93-7f33-4169-b04a-d33f120a0917
# ╠═5f6ff7a1-4165-4ce3-9c3a-fa0e47859c11
# ╠═4d92eb27-f7c8-4f8a-8eb0-3d0a11463b2c
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
