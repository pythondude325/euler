### A Pluto.jl notebook ###
# v0.18.0

using Markdown
using InteractiveUtils

# ╔═╡ dbbe934d-5ec0-4b04-af22-2e232e706966
setprecision(ceil(Int, 105/log10(2)))

# ╔═╡ 2bbfdcc6-9ab6-11ec-1377-ed6f30d30f34
x = BigFloat(2)

# ╔═╡ 5894e716-8e39-4b9a-a6d0-b1086e7c254b
issquare(x) = floor(Int,sqrt(x)) == ceil(Int,sqrt(x))

# ╔═╡ 1eed0e86-c18c-4414-b8d3-653f7adbcb48
sum(sum(parse(Int, c) for c=string(sqrt(BigFloat(x)))[[1;3:3+100-2]]) for x=filter(!issquare, 1:100))

# ╔═╡ 20abdb41-bd48-44de-9762-87803ba830b9


# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.2"
manifest_format = "2.0"

[deps]
"""

# ╔═╡ Cell order:
# ╠═dbbe934d-5ec0-4b04-af22-2e232e706966
# ╠═2bbfdcc6-9ab6-11ec-1377-ed6f30d30f34
# ╠═5894e716-8e39-4b9a-a6d0-b1086e7c254b
# ╠═1eed0e86-c18c-4414-b8d3-653f7adbcb48
# ╠═20abdb41-bd48-44de-9762-87803ba830b9
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
