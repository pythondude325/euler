### A Pluto.jl notebook ###
# v0.19.8

using Markdown
using InteractiveUtils

# ╔═╡ 8440124a-0202-11ed-0e93-5d26a2fc33dd
function digit_sum(x)
	a = 0
	while x > 0
		a += (x % 10) ^ 2
		x ÷= 10
	end
	a
end

# ╔═╡ 5e9c7dd5-d212-4e55-a5af-821f90377e98
digit_sum(89)

# ╔═╡ dc08a4fa-ade1-4dea-b81b-9569c6eccd33
function test(i)
	while !(i == 1 || i == 89)
		i = digit_sum(i)
	end
	i == 89
end

# ╔═╡ 71f71b02-7ab9-4d20-bc4d-20f5328c7a44
@assert test(44) == false

# ╔═╡ 4b6d001b-2d64-4635-b694-0781d595c3a6
@assert test(85) == true

# ╔═╡ 518249d7-0ee3-4e0d-b7ae-58ce41b7ff7e
const result = sum(test(i) for i=1:9_999_999)

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.3"
manifest_format = "2.0"

[deps]
"""

# ╔═╡ Cell order:
# ╠═8440124a-0202-11ed-0e93-5d26a2fc33dd
# ╠═5e9c7dd5-d212-4e55-a5af-821f90377e98
# ╠═dc08a4fa-ade1-4dea-b81b-9569c6eccd33
# ╠═71f71b02-7ab9-4d20-bc4d-20f5328c7a44
# ╠═4b6d001b-2d64-4635-b694-0781d595c3a6
# ╠═518249d7-0ee3-4e0d-b7ae-58ce41b7ff7e
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
