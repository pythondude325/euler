### A Pluto.jl notebook ###
# v0.18.0

using Markdown
using InteractiveUtils

# ╔═╡ 855b8f9e-9a59-11ec-1c27-8b208984fb80
const file = download("https://projecteuler.net/project/resources/p099_base_exp.txt")

# ╔═╡ 7df3f217-aa8c-4d2f-b3d9-30571f0b71e1
const input = [(parse(Int,a), parse(Int,b)) for (a,b)=split.(readlines(file), ',')]

# ╔═╡ 9bf563ba-0cb2-41fb-abb8-7216c876b0e9
f((a,b),) = b*log(a)

# ╔═╡ fece0d51-db67-454f-9bc8-c9db3cf1d369
findmax(f,input)

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.2"
manifest_format = "2.0"

[deps]
"""

# ╔═╡ Cell order:
# ╠═855b8f9e-9a59-11ec-1c27-8b208984fb80
# ╠═7df3f217-aa8c-4d2f-b3d9-30571f0b71e1
# ╠═9bf563ba-0cb2-41fb-abb8-7216c876b0e9
# ╠═fece0d51-db67-454f-9bc8-c9db3cf1d369
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
