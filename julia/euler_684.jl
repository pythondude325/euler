### A Pluto.jl notebook ###
# v0.19.8

using Markdown
using InteractiveUtils

# ╔═╡ 7c02fc71-2e80-48fd-ba3c-c096203c4165
Z(12)

# ╔═╡ a9acfb3a-829d-4208-b5be-d5735c2e6100
const m = 1_000_000_007

# ╔═╡ 4aede12e-5f65-46ca-91cb-412018035fe0
const inverse_9 = invmod(9, m)

# ╔═╡ f39c66b4-06a6-4282-8197-976e17fcda08
M(x) = mod(x, m)

# ╔═╡ 6b4587c7-48de-46a8-8e0f-d3a14418f22c
s(x) = M((x%9 + 1) * powermod(10, x÷9, m) - 1)

# ╔═╡ 99b814f2-f383-441f-9129-4887a41652fe
Madd(a,b) = M(a+b)

# ╔═╡ ce9254d0-fd3a-403d-8d98-79d1e768f989
Msum(i) = reduce(Madd, i)

# ╔═╡ ce1410ea-a9a5-4e6b-a2cf-b7dccbfa0ce7
Z2(n) = M(5 * (powermod(10, n, m) - 1) - 9*n)

# ╔═╡ f15cac26-2bce-4065-8524-5bc5c7d1f4e9
function S(k)
	# slow way to do it: reduce(Madd, s(n) for n=1:k)
	M(Z2(k ÷ 9) + Msum(s(n) for n=(k - k%9):k))
end

# ╔═╡ 65c9740b-4a34-4b44-af3c-587bec3837cf
function fib(n)
	f = Int64[]
	a = 0
	b = 1
	for i=1:n
		push!(f, a)
		a, b = b, a + b
	end
	f
end

# ╔═╡ c1d9eea5-2e9d-4dbb-b02f-9b623013c3c4
const result = Msum(S.(fib(91)[3:end]))

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.3"
manifest_format = "2.0"

[deps]
"""

# ╔═╡ Cell order:
# ╠═6b4587c7-48de-46a8-8e0f-d3a14418f22c
# ╠═7c02fc71-2e80-48fd-ba3c-c096203c4165
# ╠═4aede12e-5f65-46ca-91cb-412018035fe0
# ╠═a9acfb3a-829d-4208-b5be-d5735c2e6100
# ╠═f39c66b4-06a6-4282-8197-976e17fcda08
# ╠═99b814f2-f383-441f-9129-4887a41652fe
# ╠═ce9254d0-fd3a-403d-8d98-79d1e768f989
# ╠═ce1410ea-a9a5-4e6b-a2cf-b7dccbfa0ce7
# ╠═f15cac26-2bce-4065-8524-5bc5c7d1f4e9
# ╠═65c9740b-4a34-4b44-af3c-587bec3837cf
# ╠═c1d9eea5-2e9d-4dbb-b02f-9b623013c3c4
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
