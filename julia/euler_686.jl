### A Pluto.jl notebook ###
# v0.19.8

using Markdown
using InteractiveUtils

# ╔═╡ 45192358-d516-45c3-9fdc-15cc4ceeac7e
using FixedPointNumbers

# ╔═╡ 9f575dc0-f8b8-11ec-3d01-ebf318c5b579
UInt64

# ╔═╡ e10c7327-0dbd-4385-b840-8063756325af
log10(typemax(UInt64))

# ╔═╡ 1d797ffc-e6e5-4530-b16f-be72d81ec74d
UInt64(10)^19

# ╔═╡ feea2bfe-a497-4af4-a2a9-12f24d782138
digit_count(x)=floor(Int,log10(x)+1)

# ╔═╡ 36c11f87-e7ec-4372-814d-30b5621a66b3
digit_count(123)

# ╔═╡ f2096120-ae8c-46fc-a247-2727671bd8de
# function p(L,n)
# 	accuracy = digit_count(L)
# 	internal_digits = 18

# 	digit_diff = internal_digits - accuracy
	
# 	found = 0
# 	i = 0
# 	x = UInt64(10)^(internal_digits - 1)
# 	while found < n
# 		#print("$(x ÷ 10^digit_diff)\n")
# 		i += 1
# 		x = x * 2
# 		if x >= UInt64(10)^internal_digits
# 			x ÷= 10
# 		end
# 		if x ÷ 10^digit_diff == L
# 			found += 1
# 		end
# 	end
# 	i
# end

# ╔═╡ 42e035ea-f52b-45db-abc2-73653f1d3e82
const ten_two = N0f64(log10(2))

# ╔═╡ bac639b8-a34a-4f1d-b347-a3666704f2f0
N0f32(log10(2))

# ╔═╡ aacea65d-df52-4dcd-9c84-23193ccb655f
reinterpret(UInt64, ten_two)

# ╔═╡ c5ed18e0-b224-474c-a02a-7cbcbe147218
reinterpret(UInt64, N0f64(log10(1.23)))

# ╔═╡ 3f07fef2-f4ee-4686-bf77-dde550965dfa
reinterpret(UInt64, N0f64(log10(1.24)))

# ╔═╡ d8a377be-08b7-4e6e-b6df-11f5849db108
reinterpret(UInt64, N0f64(log(1.23)))

# ╔═╡ e0bb7a6a-2b06-4710-841a-054e88bcbb01
log(2)/log(10*exp(0))

# ╔═╡ a2c64d6c-54e7-4b1a-a50f-1846f75f8553
log10(2)

# ╔═╡ 31c56a02-c74a-48f4-b9e5-42cba20a09b1
log(10*exp(-2))

# ╔═╡ 0f03dcc5-4d3d-4146-af09-a81b0b4fbb01
function int_log(a)


end

# ╔═╡ c7e52b03-9b7f-47d2-bbe5-a19587771a0f
N0f64(log(.5*exp(1)))

# ╔═╡ 3d2f8949-fcc9-4568-8273-952c52d8e163
exp(-3)

# ╔═╡ b68da0d7-eaca-443a-b9f9-5724f9521d71
fract(x) = modf(x)[1]

# ╔═╡ 8d0bb905-55bd-4bd3-b349-4565597c02c1
function int_ln(a)
	x = N0f64(0.0)
	for _=1:10
		print("$x\n")
		x += N0f64(fract(a*N0f64(exp(-big(x)))))
		# x += a*e^-x
	end
	x
end

# ╔═╡ 261e761b-9b54-478a-8326-4ea395d79b73
reinterpret(UInt64, int_ln(1.23))

# ╔═╡ a1e9af34-8751-40f6-a1fc-f2bf86abc2a2
function p(L,n)
	A = N0f64(fract(log10(L)))
	B = N0f64(fract(log10(L + 1)))
	if B == 0
		B = N0f64(1.0)
	end

	found = 0
	i = 0
	x = N0f64(0.0)
	while found < n
		i += 1
		x += ten_two
		if A < x < B
			found += 1
		end
	end
	i
end

# ╔═╡ fec9381c-ba5b-4665-8469-d47f772fbde7
p(31415927,1)

# ╔═╡ 6921a917-5b18-4191-a288-f1888ed963a3
big(2)^2

# ╔═╡ 0abb61e2-7838-403d-9bed-4dca34267b9c
p(123,678910)

# ╔═╡ dc23d61c-3219-4918-ae27-8cb2f1fa6915
findall(is_correct.(1:1000 .* ten_two))

# ╔═╡ dfa990a4-16f6-43fb-9176-a46037375198
is_correct(ten_two * 12710)

# ╔═╡ e72639cb-2c09-4b28-9d79-b1ac081d6076
big(2)^12710

# ╔═╡ 3d162fb4-2453-43cf-b2f2-25444186ecee
10^5 == 100000

# ╔═╡ 9b0b6d19-bbc8-4919-b562-86bbb9ae59c1
function pad_num(x, digits=5)
	if x >= 10^digits
		while x >= 10^digits
			x ÷= 10
		end
		x
	else
		while x < 10^(digits-1)
			x *= 10
		end
		x
	end
end

# ╔═╡ 4b63c72c-3345-4105-b65e-3e303656ac3e
function powers_of_two(;digits=6, accuracy=3)
	i = 0
	x = 10^(digits-1)
	r = big(1)
	while true
		i += 1
		y = x * 2
		if y >= 10^digits
			x = y ÷ 10
		else
			x = y
		end
		r = r * 2

		n = pad_num(r,accuracy)
		m = x ÷ 10^(digits-accuracy)

		if n != m
			print("[$i] ($x) $m $n\n")
			break
		end
	end
	print("Broke on 2^$i\n")
	i
end

# ╔═╡ 71cee4fc-fead-4b4d-b51e-ff8a9bd551db
powers_of_two(digits=9)

# ╔═╡ f2b07e6d-3ad4-43bf-904f-6aaa057e13a8
powers_of_two()

# ╔═╡ 4f8a7d6e-df6b-4f31-be13-8fa432e8b518
break_points = [powers_of_two(digits=x) for x=3:9]

# ╔═╡ 7ea3eb7a-3465-4349-8c42-401a66fdad4e
break_points[4:end] ./ break_points[1:end-3]

# ╔═╡ 34bbc908-940d-4a46-ad7a-c7c7eff71142
join(break_points .- 1 .|> string, ",")

# ╔═╡ e07a06b2-fe82-493b-8d52-996e7bb21feb
10^10

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
FixedPointNumbers = "53c48c17-4a7d-5ca2-90c5-79b7896eea93"

[compat]
FixedPointNumbers = "~0.8.4"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.3"
manifest_format = "2.0"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"

[[deps.FixedPointNumbers]]
deps = ["Statistics"]
git-tree-sha1 = "335bfdceacc84c5cdf16aadc768aa5ddfc5383cc"
uuid = "53c48c17-4a7d-5ca2-90c5-79b7896eea93"
version = "0.8.4"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[deps.Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl", "OpenBLAS_jll"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"
"""

# ╔═╡ Cell order:
# ╠═9f575dc0-f8b8-11ec-3d01-ebf318c5b579
# ╠═e10c7327-0dbd-4385-b840-8063756325af
# ╠═71cee4fc-fead-4b4d-b51e-ff8a9bd551db
# ╠═f2b07e6d-3ad4-43bf-904f-6aaa057e13a8
# ╠═7ea3eb7a-3465-4349-8c42-401a66fdad4e
# ╠═34bbc908-940d-4a46-ad7a-c7c7eff71142
# ╠═4f8a7d6e-df6b-4f31-be13-8fa432e8b518
# ╠═1d797ffc-e6e5-4530-b16f-be72d81ec74d
# ╠═feea2bfe-a497-4af4-a2a9-12f24d782138
# ╠═36c11f87-e7ec-4372-814d-30b5621a66b3
# ╠═f2096120-ae8c-46fc-a247-2727671bd8de
# ╠═42e035ea-f52b-45db-abc2-73653f1d3e82
# ╠═45192358-d516-45c3-9fdc-15cc4ceeac7e
# ╠═bac639b8-a34a-4f1d-b347-a3666704f2f0
# ╠═aacea65d-df52-4dcd-9c84-23193ccb655f
# ╠═c5ed18e0-b224-474c-a02a-7cbcbe147218
# ╠═3f07fef2-f4ee-4686-bf77-dde550965dfa
# ╠═d8a377be-08b7-4e6e-b6df-11f5849db108
# ╠═261e761b-9b54-478a-8326-4ea395d79b73
# ╠═e0bb7a6a-2b06-4710-841a-054e88bcbb01
# ╠═a2c64d6c-54e7-4b1a-a50f-1846f75f8553
# ╠═31c56a02-c74a-48f4-b9e5-42cba20a09b1
# ╠═0f03dcc5-4d3d-4146-af09-a81b0b4fbb01
# ╠═8d0bb905-55bd-4bd3-b349-4565597c02c1
# ╠═c7e52b03-9b7f-47d2-bbe5-a19587771a0f
# ╠═3d2f8949-fcc9-4568-8273-952c52d8e163
# ╠═a1e9af34-8751-40f6-a1fc-f2bf86abc2a2
# ╠═b68da0d7-eaca-443a-b9f9-5724f9521d71
# ╠═fec9381c-ba5b-4665-8469-d47f772fbde7
# ╠═6921a917-5b18-4191-a288-f1888ed963a3
# ╠═0abb61e2-7838-403d-9bed-4dca34267b9c
# ╠═dc23d61c-3219-4918-ae27-8cb2f1fa6915
# ╠═dfa990a4-16f6-43fb-9176-a46037375198
# ╠═e72639cb-2c09-4b28-9d79-b1ac081d6076
# ╠═4b63c72c-3345-4105-b65e-3e303656ac3e
# ╠═3d162fb4-2453-43cf-b2f2-25444186ecee
# ╠═9b0b6d19-bbc8-4919-b562-86bbb9ae59c1
# ╠═e07a06b2-fe82-493b-8d52-996e7bb21feb
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
