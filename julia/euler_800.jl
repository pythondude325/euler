### A Pluto.jl notebook ###
# v0.19.8

using Markdown
using InteractiveUtils

# ╔═╡ b42270a8-fef0-11ec-0f76-43fb72d25ef9
h(p,q) = p^q * q^p

# ╔═╡ 7b7e4971-9342-4109-abad-058079431e1e
h(2,5)

# ╔═╡ 811d4cf5-8f9b-4dc5-93ac-b600f20bf191
800800 * log(800800)

# ╔═╡ bcdf0a13-67aa-4ee7-bade-9339be6acb67
5 * log(800)

# ╔═╡ 2983da94-9d0e-4025-8be4-07ad131508a9
#C(800800, 800800)

# ╔═╡ 20e02de4-4151-4a9f-9cbe-0711e054159b
const bad_answer = 1365386516

# ╔═╡ de9d6fac-6c68-4ad6-b76d-04dfdcbaea7b
log(800)

# ╔═╡ 43614129-8807-4fb6-8e4e-454e496e3722
800 * log(800)

# ╔═╡ a8a10f92-757d-498f-807c-3bb5ea35b280
800800 * log(800800)

# ╔═╡ eb761892-9c6c-48d0-87f8-caeadc591b35
lh(p,q) = q * log(p) + p * log(q)

# ╔═╡ a53f6b88-929f-4f8e-84a2-8ff792092f61
function sieve(n)
	nums = trues(n)
	nums[1] = false
	prime = 2
	while prime*prime ≤ n
		nums[prime*prime:prime:end] .= false
		prime += findfirst(@view nums[prime+1:end])
	end
	findall(nums)
end

# ╔═╡ fc182fc5-0cb2-4666-a88c-6e4dbe1f145f
const primes = sieve(20_000_000)

# ╔═╡ 41b27dc8-b80e-496a-8773-2f6930e51489
primes[1 + 13]

# ╔═╡ 62263251-8ec4-40e9-a367-dd314b66bc7c
function search_row(i, limit)
	# check the index n to see if it's a valid hybrid integer in the current row
	check(n) = lh(primes[i], primes[n]) <= limit
	
	# find an upper bound
	o = 1
	while check(i + o)
		o *= 2
	end
	# i + o is now a false upper bound

	# check if this row has no results
	if o == 1
		return 0
	end

	# now do a binary search to find the exact index where it breaks

	l = i + 1
	r = i + o

	while (r - l) > 1
		mid = (l + r) ÷ 2
		if check(mid)
			l = mid
		else
			r = mid
		end
	end

	# return the number of hybrid integers in this row
	l - i
end

# ╔═╡ 103148cf-0923-4bfa-a563-1f3b9a57f476
search_row(2, 6 * log(800))

# ╔═╡ 88c3b161-b7ef-4709-b507-eae0e9808e3b
lh(primes[end-1], primes[end])

# ╔═╡ 8656323d-85b6-4d2b-a9a8-b92e21be3fbf
function C(n, p)
	limit = p * log(n)
	count = 0
	for (i,p)=enumerate(primes)
		c = search_row(i, limit)
		if c == 0
			break
		end
		count += c
		
		# if lh(p, primes[i+1]) > V
		# 	println("Broke on $p,$(primes[i+1])")
		# 	break
		# end
		# for q=primes[i+1:end]
		# 	if lh(p,q) <= V
		# 		println("Counting $p,$q")
		# 		count += 1
		# 	else
		# 		println("Broke $p on $p,$q")
		# 		println("Count is $count")
		# 		break
		# 	end
		# end
	end
	count
end

# ╔═╡ 70fe9449-9f67-4727-9562-cda8d74fc669
C(800,1)

# ╔═╡ c6d7e203-e403-4c33-93c8-6351d7a21c1d
C(800,800)

# ╔═╡ ae5be938-7554-44a0-b595-0496131fb38b
C(800800, 800800)

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.3"
manifest_format = "2.0"

[deps]
"""

# ╔═╡ Cell order:
# ╠═b42270a8-fef0-11ec-0f76-43fb72d25ef9
# ╠═7b7e4971-9342-4109-abad-058079431e1e
# ╠═70fe9449-9f67-4727-9562-cda8d74fc669
# ╠═c6d7e203-e403-4c33-93c8-6351d7a21c1d
# ╠═ae5be938-7554-44a0-b595-0496131fb38b
# ╠═41b27dc8-b80e-496a-8773-2f6930e51489
# ╠═103148cf-0923-4bfa-a563-1f3b9a57f476
# ╠═62263251-8ec4-40e9-a367-dd314b66bc7c
# ╠═88c3b161-b7ef-4709-b507-eae0e9808e3b
# ╠═811d4cf5-8f9b-4dc5-93ac-b600f20bf191
# ╠═bcdf0a13-67aa-4ee7-bade-9339be6acb67
# ╠═2983da94-9d0e-4025-8be4-07ad131508a9
# ╠═20e02de4-4151-4a9f-9cbe-0711e054159b
# ╠═8656323d-85b6-4d2b-a9a8-b92e21be3fbf
# ╠═de9d6fac-6c68-4ad6-b76d-04dfdcbaea7b
# ╠═43614129-8807-4fb6-8e4e-454e496e3722
# ╠═a8a10f92-757d-498f-807c-3bb5ea35b280
# ╠═eb761892-9c6c-48d0-87f8-caeadc591b35
# ╠═fc182fc5-0cb2-4666-a88c-6e4dbe1f145f
# ╠═a53f6b88-929f-4f8e-84a2-8ff792092f61
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
