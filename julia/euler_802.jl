### A Pluto.jl notebook ###
# v0.19.8

using Markdown
using InteractiveUtils

# ╔═╡ ae466847-57cd-4d35-b02f-fc3186cb0b10
using Polynomials

# ╔═╡ e49b17a2-5d81-43c0-a4d4-e75094cb1bb4
using LinearAlgebra

# ╔═╡ 8a3445ca-0e40-4959-b052-9062a87820d4
const X = Polynomial{Complex{BigFloat}}(:x)

# ╔═╡ 6eb60f2d-0892-464a-89ef-abb327d3797e
const m = UInt32(1_020_340_567)

# ╔═╡ 99ffc747-07a3-43f7-8081-a2233146ac09
function starts(n)::Vector{Int64}
	s = Vector{Int64}(undef, n)
	s[1] = 2
	k = UInt64(1)
	for i=2:n
		k = (k * 2) % m
		s[i] = k
	end
	s
end

# ╔═╡ d947b2bd-effc-4784-b3fa-627f2fdd0fef
function counts(n)
	c = starts(n)
	for i=1:n
		#println("$c")
		current = c[i]
		@view(c[2i:i:end]) .= mod.(@view(c[2i:i:end]) .- current, m)
	end
	# c
	accumulate((a, b) -> (a + b) % m, c)
end

# ╔═╡ 9f4b000a-6795-486a-8c98-d8cbbb5efe0f
counts(5)

# ╔═╡ 10012227-9bf0-4eff-badb-1ec0189fab37
[
	1    2    2
	2    2    0    0
	3    4    2    2     2
	4    8    6    6     6     6
	5    16   14   14    14    14     14
	6    32   30   30    28    28     28    28
]

# ╔═╡ 2a5b76e0-1d78-42fe-9385-e43a3dd8606d
accumulate(+, [2,0,2,6,14,28])

# ╔═╡ 5dfdfad6-6883-4f2b-92f0-f1283b7629dc
const results = counts(10^7) .|> Int32

# ╔═╡ e96515ed-8c7c-4a81-88c1-bfdd5cebd704
const bad_result = 266478055

# ╔═╡ 3d6511e0-81e9-4f44-a7e6-4b4f5b018f8d
results[end]

# ╔═╡ bbde134a-b172-47bc-b293-a4e71fe5032c
function bad_unique(s)
	n = []
	for S=s
		if all(x -> !isapprox(x,S), n)
			push!(n, S)
		end
	end
	n
end

# ╔═╡ 47ab916e-eecc-44dc-bb43-f12e29b8ee5b


# ╔═╡ 63a46a4a-fdb6-11ec-0c44-2f5a94eea8d4
f((x,y)) = (x^2 - x - y^2, 2*x*y - y + π)

# ╔═╡ d4c117b3-a9ff-4dd7-b5a5-0cceb0149d7c
fc(z::Complex{BigFloat})::Complex{BigFloat} = z^2 - z + big(pi)*im

# ╔═╡ 8483a7b3-ae83-4337-9e61-66eb48d2f77a


# ╔═╡ 79402316-e295-406d-bcaa-935b3665f65e
summary(big(pi)*im)

# ╔═╡ 02e5f9b0-58ac-4ae1-91c5-bbb599e4b156
fc(z) = z^2 - z + pi*im

# ╔═╡ 0b03dba3-525d-459e-b6d0-ab3fa07fae8e
const x = Polynomial{Complex{Float64}}(:x)

# ╔═╡ 81d3aaeb-eace-498a-84a2-fce3123f1a84
const F = x^2 - x + π*im

# ╔═╡ e670842f-0b4b-4b85-8787-89361f9579e2
const bF = X^2 - X + big(pi)*im

# ╔═╡ 614c2468-a616-46f8-8574-a5c6eb3420bf
F - x

# ╔═╡ 86bc81b8-3440-43bb-991b-33fa3adcd993
F(F) - x

# ╔═╡ 88bb1324-9294-467a-9771-b00c33cfc460


# ╔═╡ beff0dce-f8ba-4475-ba5e-d7faf9c2b717
function divisors(k)
	ds = [1]
	for i=2:k-1
		if k % i == 0
			push!(ds, i)
		end
	end
	ds
end

# ╔═╡ aceab4ef-de9b-4dc2-b5a4-7d43d2199423
divisors(1_000_000)

# ╔═╡ e5ca8026-4b75-45df-920a-f822495e3917


# ╔═╡ 272e4d95-092f-4364-b35e-f35648c6789e
sum(roots(F - x))

# ╔═╡ bcbe3dcd-110d-4a99-bfa2-06a92e4313e4
sum(roots(F(F) - x))

# ╔═╡ 4616ff80-d9d5-4cc7-ad04-7ecbdce679f7


# ╔═╡ e5956c95-357a-4fa3-b534-e15d0017d671
P2(n) = 
	if n == 1
		2
	else
		big(2)^(n-1)
	end

# ╔═╡ 249a5c8c-69b2-443b-9fbb-2507f8e8b766
P2.(1:15)

# ╔═╡ 9f774d6f-2a71-4c18-bb9c-4c9232755cee
P2(10^7) % m

# ╔═╡ 1d7aff2d-dc30-40d7-aa76-e1b63b456e46
function roots_sum(p)
	c = coeffs(p)
	-c[end - 1] / c[end]
end

# ╔═╡ d6a0a5a2-40d1-4312-b225-7a857d40cc1d
[1,2,3][end-1]

# ╔═╡ a2725033-9464-44a6-8320-ddbbe207b779
2^(3-1)

# ╔═╡ 751380a6-b398-452b-ac3d-359d6d9dac90
big(1)<<(10^7) % m

# ╔═╡ 2b1c241d-e241-4d58-908d-3a29f6729b68
function funcpow(f, n)
	reduce(∘, fill(f, n))
end

# ╔═╡ cc2f4301-3cc1-4dab-a483-1fdd8d68e7cb
roots_sum(funcpow(F, 9)(x) - x)

# ╔═╡ d3a69129-5b93-4a28-9258-53d1ae3643d3
funcpow(bF, 2)(X)

# ╔═╡ e850bd86-1566-46db-9678-610186fba305
Fn(n) = funcpow(F, n)(x) - x

# ╔═╡ 73d4310b-6d3a-49fc-8aab-9405a7efece7
const r = reduce(vcat, roots.(Fn.(1:4)))

# ╔═╡ 0ffdb547-39bd-4175-a7b5-c3e254fadc59
length(r) - sum(isapprox.(r, transpose(r)) .& .! UnitUpperTriangular(trues(length(r), length(r))))

# ╔═╡ 18100c07-5856-4e8d-95ce-2c26112339c0
bad_P(n) = round(Int, real(sum(bad_unique(reduce(vcat, roots.(Fn.(1:n)))))))

# ╔═╡ 15e2ef55-abc4-400b-9766-63e233c26229
bad_P.(1:5)

# ╔═╡ d7ad1d34-7825-4301-8897-573efa4372f3
roots(Fn(2))

# ╔═╡ 9bdad3bc-e9fe-42ba-b183-63e46b277453
begin
	plot(roots(Fn(1)))
	plot!(roots(Fn(2)))
	# plot!(roots(Fn(3)))
	# plot!(roots(Fn(4)))

end

# ╔═╡ f5bcfc37-743e-447c-a3a4-6ef49957a829
roots(funcpow(F, 1)(x) - x)

# ╔═╡ 7efdb580-1ce0-4d92-ba9a-5069a2f41ddb
P(n) = Int(funcpow(F, n)(x) - x |> roots_sum)

# ╔═╡ 4094b8cb-2bbc-459b-ba7d-4d804c7193ad
P(3)

# ╔═╡ 15e79f58-bf04-410c-9571-4aeb0889e313
P(4)

# ╔═╡ 0aa6bd11-3e6e-4ab5-b308-1bb9eb1f3436
P.(1:15)

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
LinearAlgebra = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"
Polynomials = "f27b6e38-b328-58d1-80ce-0feddd5e7a45"

[compat]
Polynomials = "~3.1.4"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.3"
manifest_format = "2.0"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"

[[deps.InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[deps.Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[deps.MutableArithmetics]]
deps = ["LinearAlgebra", "SparseArrays", "Test"]
git-tree-sha1 = "4e675d6e9ec02061800d6cfb695812becbd03cdf"
uuid = "d8a4904e-b15c-11e9-3269-09a3773c0cb0"
version = "1.0.4"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"

[[deps.Polynomials]]
deps = ["LinearAlgebra", "MutableArithmetics", "RecipesBase"]
git-tree-sha1 = "5d389e6481b9d6c81d73ee9a74d1fd74f8b25abe"
uuid = "f27b6e38-b328-58d1-80ce-0feddd5e7a45"
version = "3.1.4"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.RecipesBase]]
git-tree-sha1 = "6bf3f380ff52ce0832ddd3a2a7b9538ed1bcca7d"
uuid = "3cdcf5f2-1ef4-517c-9805-6587b60abb01"
version = "1.2.1"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[deps.Test]]
deps = ["InteractiveUtils", "Logging", "Random", "Serialization"]
uuid = "8dfed614-e22c-5e08-85e1-65c5234f0b40"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl", "OpenBLAS_jll"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"
"""

# ╔═╡ Cell order:
# ╠═ae466847-57cd-4d35-b02f-fc3186cb0b10
# ╠═e49b17a2-5d81-43c0-a4d4-e75094cb1bb4
# ╠═8a3445ca-0e40-4959-b052-9062a87820d4
# ╠═6eb60f2d-0892-464a-89ef-abb327d3797e
# ╠═99ffc747-07a3-43f7-8081-a2233146ac09
# ╠═d947b2bd-effc-4784-b3fa-627f2fdd0fef
# ╠═9f4b000a-6795-486a-8c98-d8cbbb5efe0f
# ╠═10012227-9bf0-4eff-badb-1ec0189fab37
# ╠═cc2f4301-3cc1-4dab-a483-1fdd8d68e7cb
# ╠═2a5b76e0-1d78-42fe-9385-e43a3dd8606d
# ╠═15e2ef55-abc4-400b-9766-63e233c26229
# ╠═5dfdfad6-6883-4f2b-92f0-f1283b7629dc
# ╠═e96515ed-8c7c-4a81-88c1-bfdd5cebd704
# ╠═3d6511e0-81e9-4f44-a7e6-4b4f5b018f8d
# ╠═bbde134a-b172-47bc-b293-a4e71fe5032c
# ╠═73d4310b-6d3a-49fc-8aab-9405a7efece7
# ╠═47ab916e-eecc-44dc-bb43-f12e29b8ee5b
# ╠═18100c07-5856-4e8d-95ce-2c26112339c0
# ╠═0ffdb547-39bd-4175-a7b5-c3e254fadc59
# ╠═d7ad1d34-7825-4301-8897-573efa4372f3
# ╠═63a46a4a-fdb6-11ec-0c44-2f5a94eea8d4
# ╠═d4c117b3-a9ff-4dd7-b5a5-0cceb0149d7c
# ╠═8483a7b3-ae83-4337-9e61-66eb48d2f77a
# ╠═79402316-e295-406d-bcaa-935b3665f65e
# ╠═02e5f9b0-58ac-4ae1-91c5-bbb599e4b156
# ╠═0b03dba3-525d-459e-b6d0-ab3fa07fae8e
# ╠═81d3aaeb-eace-498a-84a2-fce3123f1a84
# ╠═e670842f-0b4b-4b85-8787-89361f9579e2
# ╠═d3a69129-5b93-4a28-9258-53d1ae3643d3
# ╠═614c2468-a616-46f8-8574-a5c6eb3420bf
# ╠═86bc81b8-3440-43bb-991b-33fa3adcd993
# ╠═e850bd86-1566-46db-9678-610186fba305
# ╠═9bdad3bc-e9fe-42ba-b183-63e46b277453
# ╠═88bb1324-9294-467a-9771-b00c33cfc460
# ╠═beff0dce-f8ba-4475-ba5e-d7faf9c2b717
# ╠═aceab4ef-de9b-4dc2-b5a4-7d43d2199423
# ╠═e5ca8026-4b75-45df-920a-f822495e3917
# ╠═4094b8cb-2bbc-459b-ba7d-4d804c7193ad
# ╠═f5bcfc37-743e-447c-a3a4-6ef49957a829
# ╠═272e4d95-092f-4364-b35e-f35648c6789e
# ╠═bcbe3dcd-110d-4a99-bfa2-06a92e4313e4
# ╠═4616ff80-d9d5-4cc7-ad04-7ecbdce679f7
# ╠═15e79f58-bf04-410c-9571-4aeb0889e313
# ╠═7efdb580-1ce0-4d92-ba9a-5069a2f41ddb
# ╠═0aa6bd11-3e6e-4ab5-b308-1bb9eb1f3436
# ╠═249a5c8c-69b2-443b-9fbb-2507f8e8b766
# ╠═9f774d6f-2a71-4c18-bb9c-4c9232755cee
# ╠═e5956c95-357a-4fa3-b534-e15d0017d671
# ╠═1d7aff2d-dc30-40d7-aa76-e1b63b456e46
# ╠═d6a0a5a2-40d1-4312-b225-7a857d40cc1d
# ╠═a2725033-9464-44a6-8320-ddbbe207b779
# ╠═751380a6-b398-452b-ac3d-359d6d9dac90
# ╠═2b1c241d-e241-4d58-908d-3a29f6729b68
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
