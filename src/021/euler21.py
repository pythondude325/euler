def d(n):
  total = 0
  for x in range(1, n):
    if n % x == 0:
      total += x
  
  return total


def main():
  maximum = 10000
  total = 0
  used_numbers = []

  for a in range(1, maximum+1):
    b = d(a)
    if (a not in used_numbers) and (b not in used_numbers) and (d(a) == b) and (d(b) == a) and (a != b):
      total += a
      total += b
      used_numbers.append(a)
      used_numbers.append(b)

  print total


if __name__ == "__main__":
  main()
