import csv
with open("euler22.names.txt", "r") as f:
  reader = csv.reader(f)
  names = sorted([row for row in reader][0])

  total_name_score = 0

  for pos, name in enumerate(names):
    letter_score = sum([ord(c) - 64 for c in name])
    total_name_score += letter_score * (pos+1)

  print total_name_score

# alternativly and more evily:
# print sum([sum([ord(c)-64 for c in name])*(pos+1) for pos, name in enumerate(sorted([names for names in csv.reader(open("euler22.names.txt"))][0]))])