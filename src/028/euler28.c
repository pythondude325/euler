#include <stdio.h>

// While this does work in O(n), I think i can do it in O(1)

int main(){
	int a = 1, s = 1;

	for(int d = 2; d <= 1000; d += 2){
		for(int p = 0; p < 4; p++){
			a += (s += d);
		}
	}

	printf("%d\n", a);
}


