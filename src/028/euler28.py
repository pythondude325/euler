#!/usr/bin/env python3

from fractions import Fraction

# Polynomial computed in haskell:
# polynomialSum (4*(2*x-1)^2 + 20*x) - 1
# (16 % 3)x^3+(10 % 1)x^2+(26 % 3)x+(1 % 1)

p = lambda x: 16*x**3/3 + 10*x**2 + 26*x/3 + 1

def spiral(n):
    return p((n - 1)/2)

print(spiral(Fraction(1001)))
