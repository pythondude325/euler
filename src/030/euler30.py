#!/usr/bin/env python3

numbers = [i for i in range(10, 1000000) if i == sum([int(d) ** 5 for d in str(i)])]

print("Numbers:")
print(numbers)
print(f"Sum: {sum(numbers)}")
