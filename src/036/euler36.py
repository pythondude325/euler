#!/usr/bin/env python3

s = 0
for i in range(1,1000000):
    d = str(i)
    b = bin(i)[2:]
    if b==b[::-1] and d==d[::-1]:
        s += i

print(s)
