#include <stdio.h>

int main(){
	int max_n = 0, max_n_count = -1;

	for(int n = 1; n <= 1000; n++){
		int n_count = 0;
	
		for(int a = 1; a < n; a++){
			for(int b = 1; b < n - a; b++){
				int c = n - a - b;

				if(a*a + b*b == c*c) n_count++;
			}
		}

		if(n_count > max_n_count){
			max_n = n;
			max_n_count = n_count;
		}
	}

	printf("%d\n", max_n);

	return 0;
}