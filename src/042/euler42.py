#!/usr/bin/env python3

import json
import math

is_triangle_number = lambda n: ((math.sqrt(2*n + 0.25) - 0.5) % 1) == 0

words = json.load(open("words.json", "r"))

count = 0
for word in words:
    word_sum = sum([ord(letter) - 64 for letter in word])
    if is_triangle_number(word_sum):
        count += 1

print(count)
