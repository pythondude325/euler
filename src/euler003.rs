use crate::utils::primes::PrimeIterator;
use crate::Problem;

const TARGET_NUM: i64 = 600_851_475_143;

pub struct Solution;

impl Problem for Solution {
    const NUMBER: usize = 3;

    fn solve() -> i64 {
        let prime_list = PrimeIterator::<i64>::new()
            .take_while(|&p| p * p <= TARGET_NUM)
            .collect::<Vec<_>>();

        *prime_list
            .iter()
            .rev()
            .find(|&p| TARGET_NUM % p == 0)
            .unwrap()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn check_answer() {
        assert_eq!(Solution::solve(), 6857);
    }
}
