use crate::Problem;
use num::integer::lcm;

pub struct Solution;

impl Problem for Solution {
    const NUMBER: usize = 5;

    fn solve() -> i64 {
        (1..=20).fold(1, lcm)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn check_answer() {
        assert_eq!(Solution::solve(), 232792560);
    }
}