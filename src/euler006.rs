use crate::Problem;
use num::rational::Rational;
use crate::utils::polynomial::Polynomial;
use crate::utils::faulhaber::polynomial_sum;

pub struct Solution;

impl Problem for Solution {
    const NUMBER: usize = 6;

    fn solve() -> i64 {
        let x = Polynomial::<Rational>::x();
        let p = num::pow(polynomial_sum(x.clone()), 2) - polynomial_sum(num::pow(x.clone(), 2));
        let result = p.eval(Rational::from_integer(100));
        assert!(result.is_integer());
        result.to_integer() as i64
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn check_answer() {
        assert_eq!(Solution::solve(), 25164150);
    }

    #[test]
    fn penis() {
        use num::traits::FromPrimitive;
        use num::zero;
        let x = Polynomial::<Rational>::x();
        let p = Polynomial::<Rational>::new(vec![zero::<Rational>(), Rational::from_f32(-1.5).unwrap(), Rational::from_f32(1.5).unwrap()]);
        dbg!(polynomial_sum(p));
    }
}