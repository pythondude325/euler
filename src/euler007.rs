use crate::Problem;
use crate::utils::primes::PrimeIterator;

pub struct Solution;

impl Problem for Solution {
    const NUMBER: usize = 7;

    fn solve() -> i64 {
        PrimeIterator::<i64>::new().nth(10000).unwrap()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn check_answer() {
        assert_eq!(Solution::solve(), 104743);
    }
}