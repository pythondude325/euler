use crate::Problem;

pub struct Solution;

impl Problem for Solution {
    const NUMBER: usize = 9;

    fn solve() -> i64 {
        (1..1000)
            .flat_map(|a| (a..(1000 - a)).map(move |b| (a, b, 1000 - a - b)))
            .find(|&(a, b, c)| a*a + b*b == c*c)
            .map(|(a, b, c)| a * b * c)
            .unwrap()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn check_answer() {
        assert_eq!(Solution::solve(), 31875000);
    }
}