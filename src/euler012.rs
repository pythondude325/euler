use crate::Problem;

pub struct Solution;

// Does not work on squares, but that's fine because triangle numbers are never
// square
fn count_divisors(n: u32) -> usize {
    (1..)
        .take_while(|d| d * d <= n)
        .filter(|d| n % d == 0)
        .count() * 2
}

impl Problem for Solution {
    const NUMBER: usize = 12;

    fn solve() -> i64 {
        (1..)
            .map(|i| i*(i+1)/2)
            .find(|&n| count_divisors(n) > 500)
            .unwrap() as i64
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn check_answer() {
        assert_eq!(Solution::solve(), 76576500);
    }
}