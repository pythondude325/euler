use crate::Problem;

pub struct Solution;

fn steps_in_sequence(mut n: i64) -> usize{
    let mut steps: usize = 0;
    while n > 1 {
        n = if n % 2 == 0 {
            n / 2
        } else {
            3 * n + 1
        };
        steps += 1;
    }
    steps
}

impl Problem for Solution {
    const NUMBER: usize = 14;

    fn solve() -> i64 {
        (1..1_000_000)
            .max_by_key(|&i| steps_in_sequence(i))
            .unwrap()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn check_answer() {
        assert_eq!(Solution::solve(), 837799);
    }
}