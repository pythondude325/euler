use crate::Problem;
use crate::utils::extra_math::choose;
use num::{BigInt, FromPrimitive, ToPrimitive};

pub struct Solution;

fn count_lattice_paths(n: u64) -> i64 {
    let n = BigInt::from_u64(n).unwrap();
    let result = choose(n.clone() + n.clone(), n);
    result.to_i64().unwrap()
}


impl Problem for Solution {
    const NUMBER: usize = 15;

    fn solve() -> i64 {
        count_lattice_paths(20)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn check_answer() {
        assert_eq!(Solution::solve(), 137846528820);
    }
}