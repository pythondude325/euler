use crate::Problem;
use num::FromPrimitive;

pub struct Solution;

impl Problem for Solution {
    const NUMBER: usize = 16;

    fn solve() -> i64 {
        let n = num::pow(num::BigInt::from_usize(2).unwrap(), 1000);
        n.to_radix_le(10).1.iter().map(|&c| c as i64).sum::<i64>()
    }
}


#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn check_answer() {
        assert_eq!(Solution::solve(), 1366);
    }
}