use crate::Problem;

const ONES: [&'static str; 9] = ["one", "two", "three", "four", "five", "six", "seven", "eight", "nine"];
const NTYS: [&'static str; 8] = ["twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"];
const TENS: [&'static str; 10] = ["ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"];

fn count_letters(n: usize) -> usize {
    let mut count = 0;

    if n % 1000 >= 100 {
        count += ONES[n / 100 - 1].len();
        count += "hundred".len();

        if n % 100 > 0 {
            count += "and".len();
        }
    }

    let ntys_p = n % 100 >= 20;
    let tens_p = 20 > n % 100 && n % 100 >= 10;
    let ones_p = n % 10 >= 1;

    if ntys_p {
        count += NTYS[(n % 100) / 10 - 2].len();
    } else if tens_p {
        count += TENS[(n % 100) - 10].len();
    }

    if !tens_p && ones_p {
        count += ONES[(n % 10) - 1].len();
    }

    count
}


pub struct Solution;

impl Problem for Solution {
    const NUMBER: usize = 0;

    fn solve() -> i64 {
        let count = (1..1000).map(count_letters).sum::<usize>() + "onethousand".len();

        count as i64
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn check_answer() {
        assert_eq!(Solution::solve(), 21124);
    }
}