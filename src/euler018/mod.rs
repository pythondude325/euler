use crate::Problem;

use std::cmp::max;

const INPUT: &'static str = include_str!("triangles.txt");

pub fn find_max_path(input: &'static str) -> i64 {
    let triangle: Vec<Vec<u32>> = input.lines().map(|line| {
        line.split(' ').map(|s| {
            s.parse::<u32>().expect("non-integer in file")
        }).collect()
    }).collect();

    let mut solution_row: Vec<u32> = vec![];
    let mut prev_solution_row: Vec<u32> = triangle.last().cloned().unwrap();

    for n in (0..triangle.len()-1).rev() {
        solution_row = triangle[n].iter().enumerate().map(|(i, v)| {
            v + max(prev_solution_row[i], prev_solution_row[i + 1])
        }).collect();

        prev_solution_row = solution_row.clone();
    }

    solution_row[0] as i64
}

pub struct Solution;

impl Problem for Solution {
    const NUMBER: usize = 0;

    fn solve() -> i64 {
        find_max_path(INPUT)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn check_answer() {
        assert_eq!(Solution::solve(), 1074);
    }
}