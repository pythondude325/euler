use crate::Problem;
use crate::utils::extra_math::factorial;
use num::FromPrimitive;

pub struct Solution;

impl Problem for Solution {
    const NUMBER: usize = 0;

    fn solve() -> i64 {
        let f = factorial(num::BigInt::from_usize(100).unwrap());
        f.to_radix_le(10).1.iter().map(|&d| d as i64).sum::<i64>()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn check_answer() {
        assert_eq!(Solution::solve(), 648);
    }
}