use std::cmp::Ordering;
use std::collections::BTreeSet;

use crate::Problem;

pub struct Solution;


const MAX: u32 = 28123;

fn sum_of_factors(n: u32) -> u32 {
    (1..)
        .take_while(|&f| f * f <= n)
        .filter(|&f| n % f == 0)
        .map(|f| if f * f == n { f } else { f + (n / f) })
        .sum()
}

fn test_factor_sum(n: u32) -> Ordering {
    (sum_of_factors(n) - n).cmp(&n)
}

impl Problem for Solution {
    const NUMBER: usize = 0;

    fn solve() -> i64 {
        let abundant_numbers = (1..MAX)
            .filter(|&n| test_factor_sum(n) == Ordering::Greater)
            .collect::<BTreeSet<_>>();

        let mut numbers = (0..MAX).collect::<BTreeSet<_>>();
        for &a in &abundant_numbers {
            for &b in &abundant_numbers {
                let s = a + b;
                numbers.remove(&s);
            }
        }
        numbers.iter().sum::<u32>() as i64
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn check_answer() {
        assert_eq!(Solution::solve(), 4179871);
    }
}
