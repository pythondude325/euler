use crate::Problem;
use num::ToPrimitive;

fn nth_lexicographic_permutation<T: Ord>(items: &mut [T], mut n: usize) {
    let l = items.len();
    let mut f: usize = (2..=l).product();

    for d in 0..(l - 1) {
        items[d..].sort();
        f /= l - d;
        items.swap(d, d + n / f);
        n %= f;
    }
}

pub struct Solution;

impl Problem for Solution {
    const NUMBER: usize = 0;

    fn solve() -> i64 {
        let mut i = (0..=9).collect::<Vec<u8>>();
        nth_lexicographic_permutation(&mut i, 999_999);

        let n = num::BigUint::from_radix_be(&i, 10).unwrap();
        n.to_i64().unwrap()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn check_answer() {
        assert_eq!(Solution::solve(), 2783915460);
    }
}