use crate::Problem;

pub struct Solution;

impl Problem for Solution {
    const NUMBER: usize = 25;

    fn solve() -> i64 {

        let golden_ratio = (1.0 + f32::sqrt(5.0)) / 2.0;
        let fibonacci_asymtotic = f32::log10(golden_ratio);

        let digits = f32::floor(1000.0 / fibonacci_asymtotic - 2.0);

        digits as i64
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn check_answer() {
        assert_eq!(Solution::solve(), 4782);
    }
}