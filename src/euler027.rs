use crate::Problem;
use std::collections;
use crate::utils::primes::PrimeIterator;

pub struct Solution;

impl Problem for Solution {
    const NUMBER: usize = 27;

    fn solve() -> i64 {
        let mut max_pair = (0, 0);
        let mut max_length = 0;

        let primes = PrimeIterator::new().take(1000).collect::<collections::BTreeSet<i64>>();

        for &b in primes.iter().take_while(|&&p| p <= 1000) {
            for a in 1..1000 {
                let a = -a;

                for n in 0.. {
                    let value = n*n + a*n + b;

                    if !primes.contains(&value) {
                        let final_length = n;

                        if final_length > max_length {
                            max_length = final_length;
                            max_pair = (a, b);
                        }

                        break;
                    }
                }

            }
        }
        
        max_pair.0 * max_pair.1
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn check_answer() {
        assert_eq!(Solution::solve(), -59231);
    }
}