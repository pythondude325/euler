use crate::Problem;
use num::FromPrimitive;
use itertools::Itertools;

pub struct Solution;

impl Problem for Solution {
    const NUMBER: usize = 29;

    fn solve() -> i64 {
        let mut results = (2..=100usize)
            .cartesian_product(2..=100usize)
            .map(|(a, b)| num::pow(
                num::BigInt::from_usize(a).unwrap(),
                b
            ))
            .collect::<Vec<_>>();

        results.sort();
        results.dedup();

        results.len() as i64
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn check_answer() {
        assert_eq!(Solution::solve(), 9183);
    }
}