use crate::Problem;
use std::cmp::{Ord, Ordering};

const COINS: [u32; 8] = [200, 100, 50, 20, 10, 5, 2, 1];
const TARGET: u32 = 200;

fn count(working_total: u32, coins: &[u32]) -> u32 {
    let mut counter = 0;

    for (index, coin) in coins.iter().enumerate() {
        let total = working_total + coin;

        counter += match total.cmp(&TARGET) {
            Ordering::Equal => 1,
            Ordering::Less => count(total, &coins[index..]),
            Ordering::Greater => 0
        }
    }

    counter

    // Faster than coins.iter().enumerate().map(...).sum()
}

pub struct Solution;

impl Problem for Solution {
    const NUMBER: usize = 31;

    fn solve() -> i64 {
        count(0, &COINS) as i64
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn check_answer() {
        assert_eq!(Solution::solve(), 73682);
    }
}