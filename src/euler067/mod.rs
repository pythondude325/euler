use crate::Problem;
use super::euler018;
pub struct Solution;

const INPUT: &'static str = include_str!("triangle.txt");

impl Problem for Solution {
    const NUMBER: usize = 0;

    fn solve() -> i64 {
        euler018::find_max_path(INPUT)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn check_answer() {
        assert_eq!(Solution::solve(), 7273);
    }
}