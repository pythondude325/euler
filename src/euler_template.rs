use crate::Problem;

pub struct Solution;

impl Problem for Solution {
    const NUMBER: usize = 0;

    fn solve() -> i64 {
        // solution code
        0
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn check_answer() {
        assert_eq!(Solution::solve(), 0);
    }
}