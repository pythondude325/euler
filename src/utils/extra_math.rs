use std::{iter, ops};

pub fn factorial<N>(k: N) -> N
where
    N: ops::Add<Output = N> + PartialOrd + Clone + num::One + iter::Product + num::ToPrimitive,
{
    num::range_inclusive(N::one(), k).product()
}

pub fn falling_factorial<N>(x: N, n: N) -> N
where
    N: ops::Add<Output = N>
        + ops::Sub<Output = N>
        + PartialOrd
        + Clone
        + num::Zero
        + num::One
        + iter::Product
        + num::ToPrimitive,
{
    // num::range(x.clone() - n, x).product()
    num::range(num::zero::<N>(), n).map(|k| x.clone() - k).product()
}

pub fn choose<N>(n: N, k: N) -> N
where
    N: num::One + PartialOrd + Clone + num::traits::NumOps + iter::Product<N> + num::ToPrimitive,
{
    num::range_inclusive(k.clone() + N::one(), n.clone()).product::<N>()
        / num::range_inclusive(N::one(), n - k).product::<N>()
}

#[cfg(test)]
mod tests {
    // use super::*;

    #[test]
    fn test_falling_factorial(){
        // TODO: write some tests
    }
}