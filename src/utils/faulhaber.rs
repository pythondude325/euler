use super::extra_math::{choose, factorial, falling_factorial};
use super::polynomial::Polynomial;
use num::rational::{Ratio, Rational};
use num::FromPrimitive;

pub fn bernoulli_number<T>(m: usize) -> Ratio<T>
where
    T: FromPrimitive + num::Integer + num::One + Clone,
    Ratio<T>: num::One,
{
    (0..=m)
        .map(|k| {
            (0..=k)
                .map(|v| {
                    num::one::<Ratio<T>>()
                        * Ratio::from_integer(T::from_isize(num::pow(-1, v)).unwrap())
                        * Ratio::from_integer(T::from_usize(choose(k, v)).unwrap())
                        * Ratio::from_integer(T::from_usize(num::pow(v + 1, m)).unwrap())
                        / T::from_usize(k + 1).unwrap()
                })
                .sum::<Ratio<T>>()
        })
        .sum()
}

pub fn faulhaber(p: usize) -> Polynomial<Rational> {
    num::pow(Polynomial::<Rational>::x(), p + 1) / Rational::from_usize(p + 1).unwrap()
        + num::pow(Polynomial::<Rational>::x(), p) / Rational::from_usize(2).unwrap()
        + (2..=p)
            .map(|k| {
                num::pow(Polynomial::<Rational>::x(), p - k + 1)
                    * bernoulli_number(k)
                    * Rational::from_integer(falling_factorial(p as isize, (k as isize) - 1))
                    / Rational::from_integer(factorial(k as isize))
            })
            .sum::<Polynomial<Rational>>()
}

pub fn polynomial_sum(p: Polynomial<Rational>) -> Polynomial<Rational> {
    p.coeffs()
        .iter()
        .enumerate()
        .map(|(p, c)| faulhaber(p) * c.clone())
        .sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_bernoulli_numbers() {
        assert_eq!(bernoulli_number(0), Rational::from_integer(1));
        assert_eq!(bernoulli_number(1), Rational::new_raw(1, 2));
        assert_eq!(bernoulli_number(2), Rational::new_raw(1, 6));
        assert_eq!(bernoulli_number(3), Rational::from_integer(0));
        assert_eq!(bernoulli_number(4), Rational::new_raw(-1, 30));
        assert_eq!(bernoulli_number(5), Rational::from_integer(0));
        assert_eq!(bernoulli_number(6), Rational::new_raw(1, 42));
        assert_eq!(bernoulli_number(7), Rational::from_integer(0));
        assert_eq!(bernoulli_number(8), Rational::new_raw(-1, 30));
        assert_eq!(bernoulli_number(9), Rational::from_integer(0));
        assert_eq!(bernoulli_number(10), Rational::new_raw(5, 66));
    }

    #[test]
    fn test_faulhaber() {
        assert_eq!(
            faulhaber(1),
            Polynomial::new(vec![
                Rational::from_integer(0),
                Rational::new_raw(1, 2),
                Rational::new_raw(1, 2)
            ])
        );
        assert_eq!(
            faulhaber(2),
            Polynomial::new(vec![
                Rational::from_integer(0),
                Rational::new_raw(1, 6),
                Rational::new_raw(1, 2),
                Rational::new_raw(1, 3),
            ])
        );
    }
}
