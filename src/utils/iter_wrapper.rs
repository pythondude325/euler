pub struct IterWrapper<State, Func, Item> where Func: Fn(&State) -> (Option<Item>, State) {
    state: State,
    func: Func,
}

impl<State, Func, Item> IterWrapper<State, Func, Item> where Func: Fn(&State) -> (Option<Item>, State) {
    pub fn new(initial_state: State, func: Func) -> IterWrapper<State, Func, Item> {
        IterWrapper {
            state: initial_state,
            func
        }
    }
}

impl<State, Func, Item> Iterator for IterWrapper<State, Func, Item> where Func: Fn(&State) -> (Option<Item>, State) {
    type Item = Item;
    fn next(&mut self) -> Option<Item> {
        let (output, next_state) = (self.func)(&self.state);
        self.state = next_state;
        output
    }
}
