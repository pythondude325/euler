pub(crate) mod iter_wrapper;
pub(crate) mod extra_math;
pub(crate) mod primes;
pub(crate) mod polynomial;
pub(crate) mod faulhaber;