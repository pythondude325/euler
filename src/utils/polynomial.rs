use itertools::{EitherOrBoth, Itertools};
use num::{FromPrimitive, traits::zero};
use std::{iter, ops};

#[derive(Debug, Clone, PartialEq)]
pub struct Polynomial<K> {
    coeffs: Vec<K>,
}

impl<K> Polynomial<K> {
    pub fn new(coeffs: Vec<K>) -> Polynomial<K> {
        Polynomial { coeffs }
    }

    pub fn eval<N>(&self, x: N) -> N
    where
        N: Clone + num::One + iter::Sum + ops::Mul<K, Output = N>,
        K: Clone,
    {
        self.coeffs
            .iter()
            .enumerate()
            .map(|(p, c)| num::pow(x.clone(), p) * c.clone())
            .sum()
    }

    pub fn coeffs(&self) -> &[K] {
        &self.coeffs
    }

    pub fn x() -> Polynomial<K>
    where
        K: num::Zero + num::One,
    {
        Polynomial::new(vec![K::zero(), K::one()])
    }
}

impl<K> ops::Add<K> for Polynomial<K>
where
    K: ops::Add<Output = K>,
{
    type Output = Polynomial<K>;

    fn add(self, other: K) -> Polynomial<K> {
        self + Polynomial::new(vec![other])
    }
}

impl<K> ops::Add for Polynomial<K>
where
    K: ops::Add<Output = K>,
{
    type Output = Polynomial<K>;

    fn add(self, other: Polynomial<K>) -> Polynomial<K> {
        Polynomial::new(
            self.coeffs
                .into_iter()
                .zip_longest(other.coeffs.into_iter())
                .map(|o| match o {
                    EitherOrBoth::Both(a, b) => a + b,
                    EitherOrBoth::Left(a) => a,
                    EitherOrBoth::Right(b) => b,
                })
                .collect(),
        )
    }
}

impl<K> num::Zero for Polynomial<K>
where
    K: num::Zero,
{
    fn zero() -> Polynomial<K> {
        Polynomial::new(vec![])
    }
    fn is_zero(&self) -> bool {
        self.coeffs().iter().all(|c| c.is_zero())
    }
}

impl<K> std::iter::Sum for Polynomial<K>
where
    K: num::Zero,
{
    fn sum<I: Iterator<Item = Polynomial<K>>>(iter: I) -> Polynomial<K> {
        iter.fold(zero::<Polynomial<K>>(), ops::Add::add)
    }
}

impl<K> num::One for Polynomial<K>
where
    K: Clone + num::Zero + num::One,
{
    fn one() -> Polynomial<K> {
        Polynomial::new(vec![K::one()])
    }
}

impl<K> ops::Mul<K> for Polynomial<K>
where
    K: Clone + ops::Mul<Output = K>,
{
    type Output = Polynomial<K>;

    fn mul(self, other: K) -> Polynomial<K> {
        Polynomial::new(self.coeffs.into_iter().map(|c| c * other.clone()).collect())
    }
}

impl<K> ops::Mul for Polynomial<K>
where
    K: num::Zero + Clone + ops::Mul<Output = K>,
{
    type Output = Polynomial<K>;

    fn mul(self, other: Polynomial<K>) -> Polynomial<K> {
        self.coeffs
            .into_iter()
            .enumerate()
            .map(|(p, c)| {
                let mut coeffs = Vec::new();
                coeffs.resize(p, zero::<K>());
                coeffs.append(&mut (other.clone() * c).coeffs);
                Polynomial::new(coeffs)
            })
            .sum()
    }
}

impl<K> ops::Div<K> for Polynomial<K>
where
    Polynomial<K>: ops::Mul<K, Output = Polynomial<K>>,
    K: ops::Div<Output = K> + num::One,
{
    type Output = Polynomial<K>;
    fn div(self, other: K) -> Polynomial<K> {
        self * (num::one::<K>() / other)
    }
}

impl<K> ops::Neg for Polynomial<K>
where
    K: ops::Neg<Output = K> + num::One,
    Polynomial<K>: ops::Mul<K, Output = Polynomial<K>>,
{
    type Output = Polynomial<K>;

    fn neg(self) -> Polynomial<K> {
        self * num::one::<K>().neg()
    }
}

impl<K> ops::Sub<K> for Polynomial<K>
where
    K: ops::Neg<Output = K>,
    Polynomial<K>: ops::Add<K, Output = Polynomial<K>>
{
    type Output = Polynomial<K>;
    fn sub(self, other: K) -> Polynomial<K> {
        self + -other
    }
}

impl<K> ops::Sub for Polynomial<K>
where
    Polynomial<K>: ops::Neg<Output = Polynomial<K>> + ops::Add<Output = Polynomial<K>>,
{
    type Output = Polynomial<K>;
    fn sub(self, other: Polynomial<K>) -> Polynomial<K> {
        self + (-other)
    }
}

impl<K> FromPrimitive for Polynomial<K>
where
    K: FromPrimitive
{
    fn from_i64(n: i64) -> Option<Self> {
        Some(Polynomial::<K>::new(vec![K::from_i64(n)?]))
    }

    fn from_u64(n: u64) -> Option<Self> {
        Some(Polynomial::<K>::new(vec![K::from_u64(n)?]))
    }

    // TODO: implement the rest of the types i think
}


// poly!(x; 5x^3 + 2)
macro_rules! poly {
    (x) => {
        Polynomial::x()
    };
    (($($i:tt)+)) => {
        poly!($($i)*)
    };
    ($num:literal * $b:tt) => {
        poly!($b:tt) * poly!($num)
    };
    // ($($a:tt)+ + $($b:tt)+) => {
    //     poly!($($a)*) + poly!($($b)*)
    // };
    ($num:literal) => {
        Polynomial::from_i64($num).expect("invalid value passed to polynomial macro")
    };
    ($a:tt ^ $p:literal) => {
        num::pow(poly!($a), $p)
    };
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn polynomial_operations() {
        let p: Polynomial<i32> = Polynomial::new(vec![-1, 2, 1]);
        let q: Polynomial<i32> = Polynomial::new(vec![-3, 2]);

        assert_eq!(p.eval(3), 14);
        assert_eq!(&p.eval(q).coeffs, &[2, -8, 4]);
    }

    // #[test]
    // fn polynomial_macro(){
    //     let _x: Polynomial<i32> = poly!(x);
    //     let _c: Polynomial<i32> = poly!(5);
    //     let _x2: Polynomial<i32> = poly!(x^2);
    //     let _p: Polynomial<i32> = poly!(5 * (x));
    // }
}
