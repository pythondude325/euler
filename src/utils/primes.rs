pub struct PrimeIterator<N> {
    prime_list: Vec<N>,
}

impl<N> PrimeIterator<N> {
    pub fn new() -> PrimeIterator<N> {
        PrimeIterator {
            prime_list: Vec::new(),
        }
    }
}

impl<N> Iterator for PrimeIterator<N> where N: num::Num + num::traits::NumRef + Clone + PartialOrd + num::FromPrimitive {
    type Item = N;

    fn next(&mut self) -> Option<N> {
        let first_check = self.prime_list.last().cloned().unwrap_or(N::from_u32(1).unwrap()) + N::one();

        let next_prime = num::iter::range_from(first_check)
            .find(|num| {
                self.prime_list
                    .iter()
                    .take_while(|&p| p.clone() * p <= num.clone())
                    .all(|p| num.clone() % p != N::zero())
            })
            .unwrap();

        self.prime_list.push(next_prime.clone());
        Some(next_prime)
    }
}


// pub fn factor<N>(n: N) -> Vec<N> where N: num::Num + Clone + PartialOrd + num::FromPrimitive {
//     let primes = PrimeIterator::new()
//         .take_while(|p| *p * *p <= n)
//         .collect::<Vec<_>>();

//     if let Some(factors) = primes.iter().find_map(|&p| {
//         if n % p == N::zero() {
//             let mut factors = factor(n / p);
//             factors.push(p);
//             Some(factors)
//         } else {
//             None
//         }
//     }) {
//         factors
//     } else {
//         vec![n]
//     }
// }
